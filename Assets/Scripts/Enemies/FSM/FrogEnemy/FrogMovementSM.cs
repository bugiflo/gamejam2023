using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class FrogMovementSM : StateMachine
{
    public FrogIdle Idle;

    public FrogMove Move;

    public FrogAttack Attack;

    public void InitFrogMovementSM(
        NavMeshAgent agent,
        Collider spawnArea,
        Transform player,
        GameObject bullet,
        Animator animator,
        AnimationCurve jumpCurve,
        GameObject avatarPivot,
        GameObject dropShadow,
        int numberOfAttacks, 
        Collider col,
        float maxJumpDistance
    )
    {
        Agent = agent;
        SpawnArea = spawnArea;
        Player = player;
        Bullet = bullet;
        Animator = animator;
        JumpCurve = jumpCurve;
        AvatarPivot = avatarPivot;
        DropShadow = dropShadow;
        NumberOfAttacks = numberOfAttacks;
        StartHeight = transform.position.y;
        Col = col;
        MaxJumpDistance = maxJumpDistance;

        Idle = new FrogIdle(this);
        Move = new FrogMove(this);
        Attack = new FrogAttack(this);
    }

    protected override BaseState GetInitialState()
    {
        return Idle;
    }

    public GameObject InstantiateObject(GameObject obj)
    {
        return Instantiate(obj);
    }

    public NavMeshAgent Agent { get; set; }

    public Collider SpawnArea { get; set; }

    public Transform Player { get; set; }
    
    public GameObject Bullet { get; set; }

    public Animator Animator { get; set; }

    public AnimationCurve JumpCurve { get; set; }

    public GameObject AvatarPivot { get; set; }

    public GameObject DropShadow { get; set; }

    public int NumberOfAttacks { get; set; }
    
    public float StartHeight { get; set; }
    public Collider Col { get; set; }
    
    public float MaxJumpDistance { get; set; }
}
