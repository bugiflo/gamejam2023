﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace CowChow.GameJam2023.Utilities
{
    public static class ScriptableObjectFinder
    {
        public static List<T> FindObjectsImplementingScript<T>() where T : ScriptableObject
        {
            string[] assetGuids = AssetDatabase.FindAssets("t:ScriptableObject");
            List<T> scriptableObjects = new List<T>();

            for (int i = 0; i < assetGuids.Length; i++)
            {
                string assetPath = AssetDatabase.GUIDToAssetPath(assetGuids[i]);
                T asset = AssetDatabase.LoadAssetAtPath<T>(assetPath);

                if (!asset)
                {
                    continue;
                }
                
                if (asset.GetType() == typeof(T))
                {
                    scriptableObjects.Add(asset);
                }
            }

            return scriptableObjects;
        }
    }
}