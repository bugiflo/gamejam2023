using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SwitchSceneOnButton : MonoBehaviour
{
    public Button btn;
    public string sceneName;

    private void Awake()
    {
        btn.onClick.AddListener(OnButtonPress);
    }

    void OnButtonPress()
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }

    private void OnDisable()
    {
        btn.onClick.RemoveListener(OnButtonPress);
    }
}
