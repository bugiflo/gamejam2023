
using System.Collections;
using UnityEngine;

public class MoleAttack : BaseState
{
    private MoleMovementSM _sm;
    
    public MoleAttack(MoleMovementSM stateMachine) : base("Attack", stateMachine)
    {
        _sm = stateMachine;
    }

    public override void Enter()
    {
        base.Enter();
        _sm.Animator.SetTrigger("Appear");
        _sm.StartCoroutine(AttackRoutine());
    }

    private IEnumerator AttackRoutine()
    {
        yield return new WaitForSeconds(0.5f);
        
        
        // Shoot
        for (var i = 0; i < _sm.NumberOfAttacks; i++)
        {
            var enemyTransform = _sm.transform;
            var targetDirection = _sm.Player.position - enemyTransform.position;
            targetDirection.y = _sm.Player.position.y;
            yield return new WaitForSeconds(0.5f);
            Shoot(targetDirection);
        }

        yield return new WaitForSeconds(_sm.AttackTimer);
        
        _sm.ChangeState(_sm.Idle);
    }
    
    private void Shoot(Vector3 direction)
    {
        var bulletObj = _sm.InstantiateObject(_sm.Bullet);

        bulletObj.transform.position = _sm.transform.position;
        bulletObj.transform.rotation = Quaternion.LookRotation(direction);
    }
}