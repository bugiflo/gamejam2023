﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace CowChow.GameJam2023.Utilities
{
    public class SceneUiGenerator : MonoBehaviour
    {

        public GameStateManager sceneLoader; // Reference to the SceneLoader script
        public string[] sceneNames; // Array of scene names to load
        public Button buttonPrefab; // Prefab for the buttons
        public string titleText; // Text for the title
        
        private void Start()
        {
            
            Transform buttonContainer; // Parent object for the buttons
            // Create a new Canvas object and set it as the parent for the buttons
            GameObject canvasObject = new GameObject("Canvas");
            canvasObject.AddComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
            canvasObject.AddComponent<CanvasScaler>();
            canvasObject.AddComponent<GraphicRaycaster>();

            buttonContainer = new RectTransform();
            
            buttonContainer.parent = canvasObject.transform;

            // Create a new Text object for the title and set its text and position
            Text title = new GameObject("Title").AddComponent<Text>();
            title.transform.SetParent(canvasObject.transform);
            title.text = titleText;
            title.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            title.fontSize = 36;
            title.rectTransform.anchoredPosition = new Vector2(0, 150);

            // Loop through the scene names and create a button for each one
            for (int i = 0; i < sceneNames.Length; i++)
            {
                // Create a new button from the prefab and set its text to the scene name
                Button button = Instantiate(buttonPrefab, buttonContainer);
                button.GetComponentInChildren<Text>().text = sceneNames[i];

                // Set the button's OnClick event to call the LoadScene method with the corresponding scene name
                string sceneName = sceneNames[i];
                // button.onClick.AddListener(() => sceneLoader.LoadScene(sceneName));
            }
        }


    }
}