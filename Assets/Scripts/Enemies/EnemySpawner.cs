
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemySpawner : MonoBehaviour
{
    public EEnemyType enemyType;
    public float spawnDelay = 0.25f;
    public bool useSpawnRadius = true;
    public float spawnRadius = 5;
    public bool spawnSingleEnemy;
    public int enemyAmount = 5;
    
    private List<GameObject> _spawnedEnemies;

    public void Start()
    {
        _spawnedEnemies = new List<GameObject>();

        StartCoroutine(spawnSingleEnemy
            ? SpawnSingleEnemy(useSpawnRadius)
            : SpawnMultipleEnemy(useSpawnRadius, enemyAmount));
    }

    public void DestroyAllSpawnedEnemies()
    {
        _spawnedEnemies.ForEach(Destroy);
    }
    
    private IEnumerator SpawnMultipleEnemy(bool useRadius, int amount)
    {
        for (var i = 0; i < amount; i++)
        {
            yield return new WaitForSeconds(spawnDelay);
            StartCoroutine(SpawnSingleEnemy(useRadius));
        }

        yield return null;
    }

    private IEnumerator SpawnSingleEnemy(bool useRadius)
    {
        yield return new WaitForSeconds(spawnDelay);

        var position = useRadius ? GetSpawnPositionInRadius(spawnRadius) : transform.position;

        var newEnemy = CreateEnemy(position);
        _spawnedEnemies.Add(newEnemy);

        yield return null;
    }

    private GameObject CreateEnemy(Vector3 position)
    {
        return enemyType switch
        {
            EEnemyType.Basic => EnemyFactory.Instance.CreateBasicEnemy(position),
            _ => EnemyFactory.Instance.CreateBasicEnemy(position)
        };
    }

    private Vector3 GetSpawnPositionInRadius(float radius)
    {
        var randomDirection = Random.insideUnitSphere * radius;
        randomDirection += transform.position;

        NavMesh.SamplePosition(randomDirection, out var hit, radius, NavMesh.AllAreas);
        return hit.position;
    }
    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, spawnRadius);
    }
}