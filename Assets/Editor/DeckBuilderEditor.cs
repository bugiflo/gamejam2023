﻿using UnityEditor;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CustomEditor(typeof(DeckBuilder))]
    public class DeckBuilderEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Setup Builder"))
            {
                DeckBuilder deckBuilder = (DeckBuilder)target;
                deckBuilder.StartBuilder();
            }
        }
    }
}