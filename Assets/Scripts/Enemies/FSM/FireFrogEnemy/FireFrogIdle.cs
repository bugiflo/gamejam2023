using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class FireFrogIdle : BaseState
{
        private readonly FireFrogMovementSM _sm;
        private bool _hasEntered;

        public FireFrogIdle(FireFrogMovementSM stateMachine) : base("Idle", stateMachine)
        {
                _sm = stateMachine;
        }
        
        public override void Enter()
        {
                if (_hasEntered) return;

                _hasEntered = true;

                var initialPosition = _sm.transform.position;
                if (!_sm.SpawnArea.bounds.Contains(_sm.transform.position))
                {
                        initialPosition = _sm.SpawnArea.bounds.center;
                }

                NavMesh.SamplePosition(initialPosition, out var hit, 100, NavMesh.AllAreas);

                _sm.transform.position = hit.position;
                _sm.StartHeight = hit.position.y;

                _sm.Animator.SetTrigger("Idle");

                _sm.StartCoroutine(StartFight());
        }
        
        private IEnumerator StartFight()
        {
                yield return new WaitForSeconds(1.0f);

                var choice = Random.Range(0, 2);

                if (choice == 1)
                {
                        _sm.ChangeState(_sm.Move); 
                }
                else
                {
                        _sm.ChangeState(_sm.NormalAttack);
                }
        }
}