﻿using System;
using UnityEngine;

namespace CowChow.GameJam2023
{
    public class CardChangeManger : MonoBehaviour
    {
        public UIDrawnCards cardManager;

        public Hand Hand;
        
        private void Start()
        {
            if (!cardManager)
            {
                cardManager = GetComponent<UIDrawnCards>();
            }
            if (cardManager != null) InitHandUI();

        }

        public void UpdateSlot(int slot)
        {
            if(!cardManager) {return;}
            cardManager.SetSpell(slot, Hand.GetSpellAtSlot(slot));
        }

        public void InitHandUI()
        {
            for (int slot = 0; slot < Hand.SlotsLength(); slot++)
            {
                cardManager.SetSpell(slot, Hand.GetSpellAtSlot(slot));

            }
        }
        
    }
}