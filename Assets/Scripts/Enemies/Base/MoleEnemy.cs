using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

public class MoleEnemy : Enemy, IDamagable
{
    public int hp;
    public int damage;
    public float bulletSpeed;
    public GameObject bullet;
    public float idleTimer;
    public float attackTimer;
    public int numberOfAttacks;
    public Animator animator;

    private int _currentHp;
    private Transform _player;
    private MoleMovementSM _sm;
    private Collider _col;

    public event Action OnDeath;

    private void Awake()
    {
        _currentHp = hp;
        _col = GetComponent<Collider>();
    }

    private void Start()
    {
        _sm = transform.AddComponent<MoleMovementSM>();

        var bulletComponent = bullet.GetComponent<FrogBullet>();
        bulletComponent.Init(damage, bulletSpeed);

        _sm.InitMoleMovementSM(_col, idleTimer, attackTimer, numberOfAttacks, _player, bullet, animator);

        _sm.RunStateMachine();
    }

    private void Update()
    {
        if (_player == null) _player = FindPlayer();
    }

    public Vector3 FindNewPosition()
    {
        return Vector3.zero;
    }

    public void Move(Vector3 position)
    {

    }

    public void CheckForPlayerDistance(Vector3 playerPosition)
    {

    }

    public Transform FindPlayer()
    {
        var player = GameObject.FindGameObjectWithTag("Player");

        _sm.Player = player.transform;

        return player != null ? player.transform : null;
    }

    public TakenDamage TakeDamage(int amount)
    {
        TakenDamage takenDamage;
        if (_currentHp <= amount)
        {
            takenDamage = new TakenDamage(_currentHp, true);
            Die();
        }
        else
        {
            takenDamage = new TakenDamage(amount, false);
            _currentHp -= amount;
        }

        return takenDamage;
    }

    public void Die()
    {
        OnDeath?.Invoke();
        Destroy(gameObject);
    }
}