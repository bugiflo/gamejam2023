using CowChow.GameJam2023;
using System;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The players health.")]
    private Health health;
    /// <summary>
    /// The players health.
    /// </summary>
    public Health Health => Health;

    [SerializeField, Tooltip("The script managing the players movement.")]
    private PlayerMovement movement;

    [SerializeField, Tooltip("The script managing the players animations.")]
    private PlayerAnimator animator;

    [SerializeField, Tooltip("The script managing the player sounds.")]
    private new PlayerAudio audio;

    [SerializeField, Tooltip("The script managing the spell casting.")]
    private PlayerSpellcaster spellManager;

    [SerializeField, Tooltip("The script handling the spell selecting.")]
    private SpellSelector spellSelector;


    // --- | MonoBehaviour | -----------------------------------------------------------------------------------------------

    private void OnEnable()
    {
        GameManager.RegisterPlayer(this);
    }

    private void OnDisable()
    {
        GameManager.UnregisterPlayer(this);
    }


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Moves the player.
    /// </summary>
    /// <param name="direction">The direction to move.</param>
    public void Move(Vector2 direction)
    {
        movement.Move(direction);
        animator.Move(direction);
    }

    /// <summary>
    /// Lets the player dash.
    /// </summary>
    /// <param name="direction">The direction to dash.</param>
    public void Dash(Vector2 direction)
    {
        movement.Dash(direction);
        animator.Dash();
        audio.Dash();
    }

    /// <summary>
    /// Cast the spell at a given position.
    /// </summary>
    /// <param name="slot">The position of the spell.</param>
    /// <param name="direction">The direction the spell was cast.</param>
    public void Cast(Vector2 direction)
    {
        spellManager.Cast(spellSelector.Current, direction);
    }
}
