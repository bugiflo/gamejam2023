﻿using UnityEngine;

namespace CowChow.GameJam2023
{
    public class PlayerSpellcaster : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The script managing the spells.")]
        private Hand handCards;

        [SerializeField, Tooltip("The object that is holding the wand.")]
        private Transform hand;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Cast a spell.
        /// </summary>
        /// <param name="slot">The slot used.</param>
        /// <param name="direction">The direction of the spell.</param>
        public void Cast(SpellSlot slot, Vector2 direction)
        {
            hand.forward = new Vector3(direction.x, 0f, direction.y);
            handCards.CastSpell((int)slot);
        }

#if UNITY_EDITOR
        [ContextMenu("Cast/First")]
        public void CastFirst() => handCards.CastSpell((int)SpellSlot.First);

        [ContextMenu("Cast/Second")]
        public void CastSecond() => handCards.CastSpell((int)SpellSlot.Second);

        [ContextMenu("Cast/Third")]
        public void CastThird() => handCards.CastSpell((int)SpellSlot.Third);

        [ContextMenu("Cast/Fourth")]
        public void CastFourth() => handCards.CastSpell((int)SpellSlot.Fourth);
#endif
    }
}