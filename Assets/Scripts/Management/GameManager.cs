using System;

public static class GameManager
{
    public static Room ActiveRoom { get; private set; }
    public static PlayerController Player { get; private set; }
    public static UIManager Ui { get; private set; }
    public static PlayerInput Inputs { get; private set; }
    public static CameraController Camera { get; private set; }

    public static void RegisterPlayer(PlayerController player)
    {
        Player = player;
    }

    public static void UnregisterPlayer(PlayerController player)
    {
        if (Player == player)
        {
            Player = null;
        }
    }

    public static void RegisterCamera(CameraController camera)
    {
        Camera = camera;
    }

    public static void UnregisterCamera(CameraController camera)
    {
        if (Camera == camera)
        {
            Camera = null;
        }
    }

    internal static void SetActiveRoom(Exit entry)
    {
        ActiveRoom.gameObject.SetActive(true);

        Room room = entry.GetRoom();

        Player.transform.position = entry.transform.position;
        Player.transform.forward = entry.transform.forward;
        Player.transform.SetParent(room.transform);
        Camera.transform.SetParent(room.transform);

        ActiveRoom = room;
        ActiveRoom.gameObject.SetActive(true);
    }
}
