using CowChow.GameJam2023;
using UnityEngine;

public class UICardSpawner : MonoBehaviour
{
    public Spell spell;

    public UIDrawnCards cardManager;

    public SpellSlot slot;

    public int position;

    [ContextMenu("Spawn in Slot")]
    public void SpawnCardAtSlot()
    {
        cardManager.SetSpell(slot, spell);
    }

    [ContextMenu("Spawn in Position")]
    public void SpawnCardAtPosition()
    {
        cardManager.SetSpell(position, spell);
    }

    [ContextMenu("Clear in Slot")]
    public void ClearCardAtSlot()
    {
        cardManager.ClearSlot(slot);
    }

    [ContextMenu("Clear in Position")]
    public void ClearCardAtPosition()
    {
        cardManager.ClearSlot(position);
    }
}
