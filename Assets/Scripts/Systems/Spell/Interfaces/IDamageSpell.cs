﻿namespace CowChow.GameJam2023
{
    public interface IDamageSpell
    {
        int Damage { get; set; }
    }
}