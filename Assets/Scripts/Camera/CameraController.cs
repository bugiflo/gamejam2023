using UnityEngine;

public class CameraController : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("Refferences")] // --------------------------------------------------------------------
    [SerializeField, Tooltip("The object to follow.")]
    private Transform target = default;

    [SerializeField, Tooltip("The controlling point of the camera.")]
    private Transform pivot = default;


    // --- | MonoBehaviour | -----------------------------------------------------------------------------------------------

    private void OnEnable()
    {
        GameManager.RegisterCamera(this);
    }

    private void OnDisable()
    {
        GameManager.UnregisterCamera(this);
    }

    private void Update()
    {
        pivot.position = target.position;
    }
}
