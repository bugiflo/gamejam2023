using UnityEngine;

public class MoleMovementSM : StateMachine
{
    public MoleIdle Idle;

    public MoleAttack Attack;

    public void InitMoleMovementSM(
        Collider col,
        float idleTimer,
        float attackTimer,
        int numberOfAttacks,
        Transform player, 
        GameObject bullet,
        Animator animator
    )
    {
        Collider = col;
        AttackTimer = attackTimer;
        IdlerTimer = idleTimer;
        Player = player;
        Bullet = bullet;
        NumberOfAttacks = numberOfAttacks;
        Animator = animator;
        
        Idle = new MoleIdle(this);
        Attack = new MoleAttack(this);
    }

    protected override BaseState GetInitialState()
    {
        return Idle;
    }
    
    public GameObject InstantiateObject(GameObject obj)
    {
        return Instantiate(obj);
    }
    
    public Collider Collider { get; set; }
    
    public float IdlerTimer { get; set; }
    
    public float AttackTimer { get; set; }
    
    public Transform Player { get; set; }
    
    public GameObject Bullet { get; set; }
    
    public int NumberOfAttacks { get; set; }

    public Animator Animator { get; set; }
}