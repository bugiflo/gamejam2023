﻿using UnityEngine;
using UnityEngine.Events;

namespace CowChow.GameJam2023
{
    public class SpellEventListener : MonoBehaviour
    {
        public SpellEvent Event;
        public UnityEvent<GameAction> Response;

        private void OnEnable()
        { Event.RegisterListener(this); }

        private void OnDisable()
        { Event.UnregisterListener(this); }

        public void OnEventRaised(GameAction action)
        { Response.Invoke(action); }   
    }
}