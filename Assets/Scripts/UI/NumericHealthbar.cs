using TMPro;
using UnityEngine;

public class NumericHealthbar : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The health to display.")]
    private Health health = default;

    [SerializeField, Tooltip("The ui element to display the text.")]
    private TMP_Text textField = default;


    // --- | MonoBehaviour | -----------------------------------------------------------------------------------------------

    private void OnEnable()
    {
        RegisterHealth(health);
    }

    private void OnDisable()
    {
        UnregsiterHealth(health);
    }


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    public void SetHealth(Health health)
    {
        if (enabled)
        {
            UnregsiterHealth(this.health);
        }
        this.health = health;
        if (enabled)
        {
            RegisterHealth(this.health);
        }
    }

    private void UpdateDisplay(int currenthealth, int maxHealth)
    {
        textField.SetText($"{currenthealth} / {maxHealth}");
    }

    private void RegisterHealth(Health health)
    {
        if (this.health)
        {
            health.OnUpdate += UpdateDisplay;
            UpdateDisplay(health.Current, health.Max);
        }
        else
        {
            UpdateDisplay(0, 0);
        }
    }

    private void UnregsiterHealth(Health health)
    {
        if (health)
        {
            health.OnUpdate -= UpdateDisplay;
        }
    }
}
