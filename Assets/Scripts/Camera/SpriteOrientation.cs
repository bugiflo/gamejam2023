using UnityEngine;

public class SpriteOrientation : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The avatar to rotate.")]
    private Transform avatar = default;


    // --- | MonoBehaviour | -----------------------------------------------------------------------------------------------

    private void Start()
    {
        if (GameManager.Camera)
        {
            avatar.transform.forward = GameManager.Camera.transform.forward;
        }
    }
}
