﻿using System.Collections.Generic;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CreateAssetMenu(fileName = "Redraw Action", menuName = "Spell System/Actions/Redraw", order = 0)]
    public class RedrawAction : LocalAction
    {
        [SerializeField] private Hand hand;

        public override void Activate()
        {
            hand.RedrawHand();
            base.Activate();
        }
    }
}