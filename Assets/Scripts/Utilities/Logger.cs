﻿using UnityEngine;

namespace CowChow.GameJam2023.Utilities
{
    public class Logger : MonoBehaviour
    {
         public void LogInt(int intValue)
    {
        Debug.Log("intValue: " + intValue);
    }

    public void LogFloat(float floatValue)
    {
        Debug.Log("floatValue: " + floatValue);
    }

    public void LogBool(bool boolValue)
    {
        Debug.Log("boolValue: " + boolValue);
    }

    public void LogString(string stringValue)
    {
        Debug.Log("stringValue: " + stringValue);
    }

    public void LogGameObject(GameObject gameObjectValue)
    {
        Debug.Log("gameObjectValue: " + gameObjectValue.name);
    }

    public void LogTransform(Transform transformValue)
    {
        Debug.Log("transformValue position: " + transformValue.position);
        Debug.Log("transformValue rotation: " + transformValue.rotation);
        Debug.Log("transformValue scale: " + transformValue.localScale);
    }

    public void LogVector2(Vector2 vector2Value)
    {
        Debug.Log("vector2Value x: " + vector2Value);
    }

    public void LogVector3(Vector3 vector3Value)
    {
        Debug.Log("vector3Value x: " + vector3Value);
    }

    public void LogColor(Color colorValue)
    {
        Debug.Log("colorValue r: " + colorValue);
    }

    public void LogQuaternion(Quaternion quaternionValue)
    {
        Debug.Log("quaternionValue x: " + quaternionValue);
    }

    public void LogRect(Rect rectValue)
    {
        Debug.Log("rectValue x: " + rectValue.x);
        Debug.Log("rectValue y: " + rectValue.y);
        Debug.Log("rectValue width: " + rectValue.width);
        Debug.Log("rectValue height: " + rectValue.height);
    }

    public void LogBounds(Bounds boundsValue)
    {
        Debug.Log("boundsValue center: " + boundsValue.center);
        Debug.Log("boundsValue extents: " + boundsValue.extents);
    }

    public void LogRay(Ray rayValue)
    {
        Debug.Log("rayValue origin: " + rayValue.origin);
        Debug.Log("rayValue direction: " + rayValue.direction);
    }

    public void LogLayerMask(LayerMask layerMaskValue)
    {
        Debug.Log("layerMaskValue value: " + layerMaskValue.value);
    }
    }
}