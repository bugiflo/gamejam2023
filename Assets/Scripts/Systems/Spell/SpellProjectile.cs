using System;
using System.Collections;
using System.Collections.Generic;
using CowChow.GameJam2023;
using JetBrains.Annotations;
using UnityEngine;

public class SpellProjectile : MonoBehaviour, IProjectile
{
    [SerializeField] private float MaxSpeed = 50;
    
    [SerializeField] private int damage = 1;
    
    [SerializeField] private Rigidbody _rigidbody;

    public Rigidbody Rb
    {
        get => _rigidbody;
        set => _rigidbody = value;
    }

    [SerializeField] private float lifetime = 2;
    public float Lifetime
    {
        get => lifetime;
        set => lifetime = value;
    }

    [SerializeField]  private bool timerActive = false;
    [SerializeField]  private float timer = 0;
    
    public float LaunchForce { get; set; }
    
    public event Action OnEndOfLife;
    public event Action<Collision> OnProjectileCollision;

    private void OnEnable()
    {
        SetUpProjectileRb();
    }


    private void Update()
    {
        RunTimer();
    }

    public void SetUpProjectileRb()
    {
        if (_rigidbody)
        {
            return;
        }
        _rigidbody = GetComponent<Rigidbody>();
        if (_rigidbody == null)
        {
            Debug.LogError($"{this} needs a rigidbody to work as {typeof(IProjectile)}");
        }
    }

    public void Shoot(Vector3 direction)
    {
        StartTimer();
        _rigidbody.AddForce(direction * Mathf.Clamp(LaunchForce, 1, MaxSpeed), ForceMode.Impulse);

    }

    private void StartTimer()
    {
        timer = 0;
        timerActive = true;
    }
    
    private void RunTimer()
    {
        if(!timerActive) return;
        if (timer < lifetime)
        {
            timer += Time.deltaTime;
            return;
        }
        OnTimerElapsed();
    }

    private void OnTimerElapsed()
    {
        timerActive = false;
        EndProjectileLife();
    }

    private void OnCollisionEnter(Collision collision)
    {
        OnCollisionWithDamagable(collision.gameObject.GetComponent<IDamagable>());
        
        OnProjectileCollision?.Invoke(collision);        

        EndProjectileLife();
    }

    private bool OnCollisionWithDamagable([CanBeNull] IDamagable entity)
    {
        if (entity == null)
        {
            return false;
        }

        entity.TakeDamage(damage);
        return true;
    }

    private void EndProjectileLife()
    {
        gameObject.SetActive(false);
        
        OnEndOfLife?.Invoke();
        Destroy(gameObject);
    }
}
