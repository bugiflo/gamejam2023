using CowChow.GameJam2023;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class UIDrawnCards : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The wrapper for all slots.")]
    private RectTransform container;

    [SerializeField, Tooltip("The blueprint for all slots.")]
    private UISlot slotPrefab;

    [SerializeField, Tooltip("The collections of cards that can be displayed.")]
    private CardCollection cardCollection;

    [SerializeField, Tooltip("The slots in the ui.")]
    private List<UISlot> slots = new List<UISlot>();

    [Space, SerializeField, Tooltip("Called before elements in the ui get updated.")]
    private UnityEvent beforeUpdate = default;
    /// <summary>
    /// Called before elements in the ui get updated.
    /// </summary>
    public event UnityAction BeforeUpdate
    {
        add => beforeUpdate.AddListener(value);
        remove => beforeUpdate.RemoveListener(value);
    }


    // --- | Properties | --------------------------------------------------------------------------------------------------

    private float slotWidth;
    private int minSlots;
    private int highlightedSlot = 0;


    // --- | MonoBehaviour | -----------------------------------------------------------------------------------------------

    private void Awake()
    {
        slotWidth = slotPrefab.RectTransform.rect.width;
        minSlots = slots.Count;
        slots[highlightedSlot].AddHighlight();
    }


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Sets the content of a slot.
    /// </summary>
    /// <param name="position">The position to put the spell.</param>
    /// <param name="spell">The spelll to set.</param>
    public void SetSpell(int position, Spell spell)
    {
        beforeUpdate.Invoke();
        if (position >= slots.Count)
        {
            AddNewSlots(position + 1);
        }
        RemoveCard(position);
        slots[position].AddCard(Instantiate(cardCollection[spell]));
    }
    /// <summary>
    /// Sets the content of a slot.
    /// </summary>
    /// <param name="position">The position to put the slot.</param>
    /// <param name="spell">The spell to insert.</param>
    public void SetSpell(SpellSlot position, Spell spell) => SetSpell((int)position, spell);

    /// <summary>
    /// Clears a slot by removing its card.
    /// </summary>
    /// <param name="position">The position to clear.</param>
    public void ClearSlot(int position)
    {
        beforeUpdate.Invoke();
        RemoveCard(position);
        RemoveExistingsSlots(position);
    }
    /// <summary>
    /// Removes the card form a slot.
    /// </summary>
    /// <param name="slot">The slot to clear.</param>
    public void ClearSlot(SpellSlot slot) => ClearSlot((int)slot);

    /// <summary>
    /// Highlights the spell at the given position.
    /// </summary>
    /// <param name="position">The position to highlight.</param>
    public void HighlightSpell(int position)
    {
        beforeUpdate.Invoke();
        slots[highlightedSlot].RemoveHighlight();
        highlightedSlot = position;
        slots[highlightedSlot].AddHighlight();
    }
    /// <summary>
    /// Highlights the spell at the given slot.
    /// </summary>
    /// <param name="slot">The slot whos spell to highlight.</param>
    public void HighlightSpell(SpellSlot slot) => HighlightSpell((int)slot);

    private void AddNewSlots(int newSize)
    {
        float indexOffset = slotWidth * (newSize - 1) * 0.5f;
        for (int i = 0; i < newSize; i++)
        {
            if (i >= slots.Count)
            {
                slots.Add(Instantiate(slotPrefab, container));
            }
            slots[i].RectTransform.anchoredPosition = Vector2.right * (slotWidth * i - indexOffset);
        }
    }

    private void RemoveExistingsSlots(int removedSlot)
    {
        for (int i = removedSlot; i >= minSlots && slots[i].Card == null; i--)
        {
            Destroy(slots[i].gameObject);
            slots.RemoveAt(i);
        }
        float indexOffset = slotWidth * (slots.Count - 1) * 0.5f;
        for (int i = 0; i < slots.Count; i++)
        {
            slots[i].RectTransform.anchoredPosition = Vector2.right * (slotWidth * i - indexOffset);
        }
    }

    private void RemoveCard(int position)
    {
        Destroy(slots[position].RemoveCard()?.gameObject);
    }
}
