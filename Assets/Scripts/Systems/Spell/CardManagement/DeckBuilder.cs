﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CowChow.GameJam2023
{
    public class DeckBuilder : MonoBehaviour
    {
        enum BuidlStep
        {
            None,
            SelectFromBooster,
            SelectOld
        }

        [SerializeField] private BuidlStep currentBuidlStep = BuidlStep.None;
        
        public Deck playerDeck;
        public List<Spell> oldSpells;
        public List<Spell> newSpells;
        public BoosterPack pack;

        public UIDrawnCards packUI;
        private int packHighlightedInd = 0;
        
        public UIDrawnCards deckUI;
        private int deckHighlightedInd = 0;
        

        [SerializeField] private Spell pickedNew;
        [SerializeField] private Spell pickedOld;

        public SoEvent OnBuildingFinished;


        private void Start()
        {
            HideUI();
        }

        public void FinishDeckBuilding()
        {
            ReplaceCards();

            OnBuildingFinished.Raise();
            
            HideUI();
            currentBuidlStep = BuidlStep.None;
            Time.timeScale = 1;
        }

        private void ReplaceCards()
        {
            if (pickedNew && pickedOld)
            {
                playerDeck.Replace(pickedNew, deckHighlightedInd);
            }
        }

        public void HideUI()
        {
            HideBoosterUI();
            HideDeckUI();
        }

        private void HideDeckUI()
        {
            deckUI.gameObject.SetActive(false);
        }

        private void HideBoosterUI()
        {
            packUI.gameObject.SetActive(false);
        }

        private void ResetSelection()
        {
            pickedNew = null;
            pickedOld = null;
        }

        public void StartBuilder()
        {
            ResetSelection();
            SetupBooster();
            SetupDeck();
            
            DisplayBooster();
            currentBuidlStep = BuidlStep.SelectFromBooster;
            Time.timeScale = 0;
        }

        private void SetupBooster()
        {
            newSpells = pack.GenerateBooster();
        }

        public void DisplayBooster()
        {
            packUI.gameObject.SetActive(true);
            for (int i = 0; i < newSpells.Count; i++)
            {
                Spell newSpell = newSpells[i];

                packUI.SetSpell(i, newSpell);
            }

            packUI.HighlightSpell(packHighlightedInd);
        }

        public void CycleBoosterUp()
        {
            CycleBooster(1);
        }
        
        public void CycleBoosterDown()
        {
            CycleBooster(-1);
        }

        private void CycleBooster(int count)
        {
            if(currentBuidlStep != BuidlStep.SelectFromBooster) return;
            
            packHighlightedInd += count;
            if (packHighlightedInd < 0)
            {
                packHighlightedInd = newSpells.Count - 1;
            }

            if (packHighlightedInd >= newSpells.Count)
            {
                packHighlightedInd = 0;
            }
            packUI.HighlightSpell(packHighlightedInd);
        }

        public void CycleDeckUp()
        {
            CycleDeck(1);
        }
        
        public void CycleDeckDown()
        {
            CycleDeck(-1);
        }
        
        private void CycleDeck(int count)
        {
            if(currentBuidlStep != BuidlStep.SelectOld) return;
            
            deckHighlightedInd += count;
            deckHighlightedInd %= oldSpells.Count;
            if (deckHighlightedInd < 0)
            {
                deckHighlightedInd = oldSpells.Count - 1;
            }
            deckUI.HighlightSpell(deckHighlightedInd);
        }
        
        private void SetupDeck()
        {
            oldSpells = playerDeck.CardsInDeck;
        }

        private void DisplayDeck()
        {
            deckUI.gameObject.SetActive(true);
            for (int i = 0; i < oldSpells.Count; i++)
            {
                Spell oldSpell = oldSpells[i];

                deckUI.SetSpell(i, oldSpell);
            }
            deckUI.HighlightSpell(deckHighlightedInd);
            currentBuidlStep = BuidlStep.SelectOld;
        }

        public void SelectNewSpell()
        {
            if(currentBuidlStep != BuidlStep.SelectFromBooster) return;
            pickedNew = newSpells[packHighlightedInd];
            HideBoosterUI();
            DisplayDeck();
        }

        public void SelectOldSpell()
        {
            if(currentBuidlStep != BuidlStep.SelectOld) return;
         
            pickedOld = oldSpells[packHighlightedInd];
            FinishDeckBuilding();
        }
    }
}