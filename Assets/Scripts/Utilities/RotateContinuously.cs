using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateContinuously : MonoBehaviour
{
    public float rotationSpeed = 50f; // Rotation speed in degrees per second

    // Update is called once per frame
    void Update()
    {
        // Calculate the rotation amount based on deltaTime
        float rotationAmount = rotationSpeed * Time.deltaTime;

        // Rotate the object around the y-axis by the calculated amount
        transform.Rotate(Vector3.up, rotationAmount);
    }
}
