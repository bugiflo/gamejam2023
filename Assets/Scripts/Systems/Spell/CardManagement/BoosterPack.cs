﻿using System.Collections.Generic;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CreateAssetMenu(fileName = "Default Booster", menuName = "Spell System/Booster", order = 0)]
    public class BoosterPack : ScriptableObject
    {
        [SerializeField] private int boosterSize = 3;
        
        [SerializeField] private Deck boosterSource;

        public List<Spell> GenerateBooster()
        {
            List<Spell> cards = new List<Spell>();
            for (int i = 0; i < boosterSize; i++)
            {
                cards.Add(boosterSource.GetRandomSpell());
            }

            return cards;
        }
        
    }
}