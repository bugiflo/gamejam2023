using UnityEngine;

public class UISlot : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The offset of the card when highlighted.")]
    private float highlightOffset = 50;


    // --- | Properties | --------------------------------------------------------------------------------------------------

    /// <summary>
    /// The transform component of the slot.
    /// </summary>
    public RectTransform RectTransform => (RectTransform)transform;

    /// <summary>
    /// The card in the slot.
    /// </summary>
    public UICard Card { get; private set; }


    /// <summary>
    /// True if the slot is highlighted.
    /// </summary>
    public bool IsHighlighted { get; private set; }


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Puts the card inside the slot.
    /// </summary>
    /// <param name="card"></param>
    public void AddCard(UICard card)
    {
        Card = card;
        card.RectTransform.SetParent(RectTransform);
        card.RectTransform.localScale = Vector3.one;
        UpdateCardPosition();
    }

    /// <summary>
    /// Removes a card from the slot.
    /// </summary>
    /// <returns>The removed card.</returns>
    public UICard RemoveCard()
    {
        UICard card = Card;
        if (card != null)
        {
            Card.RectTransform.SetParent(null);
            Card = null;
        }
        return card;
    }

    /// <summary>
    /// Sets the highlight state of the slot.
    /// </summary>
    /// <param name="state">True if the slot should be highlighted.</param>
    public void UpdateHighlight(bool state)
    {
        IsHighlighted = state;
        if (Card)
        {
            UpdateCardPosition();
        }
    }

    /// <summary>
    /// Highlights the card in the slot.
    /// </summary>
    [ContextMenu("Highlight/Add")]
    public void AddHighlight() => UpdateHighlight(true);

    /// <summary>
    /// Highlights the card in the slot.
    /// </summary>
    [ContextMenu("Highlight/Remove")]
    public void RemoveHighlight() => UpdateHighlight(false);

    private void UpdateCardPosition()
    {
        if (IsHighlighted)
        {
            Card.RectTransform.anchoredPosition = Vector2.zero;
        }
        else
        {
            Card.RectTransform.anchoredPosition = Vector2.down * highlightOffset;
        }
    }
}
