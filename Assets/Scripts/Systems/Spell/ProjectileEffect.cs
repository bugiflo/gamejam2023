﻿using System;
using CowChow.GameJam2023.Utilities;
using UnityEngine;

namespace CowChow.GameJam2023
{
    public class ProjectileEffect : MonoBehaviour
    {
        private IProjectile _projectile;

        public GameObject Effect;

        public float effectDestructionDelay;

        private void Start()
        {
            if (_projectile == null)
            {
                _projectile = GetComponent<IProjectile>();
            }

            if (_projectile == null)
            {
                return;
            }

            _projectile.OnEndOfLife += SpawnEffect;
        }

        private void SpawnEffect()
        {
            if (Effect)
            {
                GameObject newEffect = Instantiate(Effect, transform.position, transform.rotation);
                newEffect
                    .AddComponent<DelayedSelfDestruct>()
                    .destroyDelay = effectDestructionDelay;
                newEffect.transform.Rotate(Effect.transform.rotation.eulerAngles);
            }
        }

        
    }
}