using System;
using UnityEngine;
using UnityEngine.Events;

public class Exit : MonoBehaviour
{
    [SerializeField, Tooltip("The exit where this one leads to.")]
    private Exit other = default;

    [SerializeField, Tooltip("The object blocking the exit.")]
    private GameObject door = default;

    [Space, SerializeField, Tooltip("Called when the room is left.")]
    private UnityEvent<Exit> onLeave = default;
    /// <summary>
    /// Called when the room is left.
    /// </summary>
    public event UnityAction<Exit> OnLeave
    {
        add => onLeave.AddListener(value);
        remove => onLeave.RemoveListener(value);
    }

    // --- | Properties | --------------------------------------------------------------------------------------------------

    private bool isCloesd = false;
    private Room room;


    // --- | MonoBehaviour | -----------------------------------------------------------------------------------------------

    private void Awake()
    {
        room = transform.GetComponentInParent<Room>();
        if (!room)
        {
            Debug.LogError("No room assigend for entry.", this);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            onLeave?.Invoke(this.other);
        }
    }


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    public Room GetRoom()
    {
        return room;
    }

    /// <summary>
    /// Opens the exit.
    /// </summary>
    [ContextMenu("Open")]
    public void Open()
    {
        isCloesd = false;
        UpdateDoor();
    }

    /// <summary>
    /// Closes the exit.
    /// </summary>
    [ContextMenu("Close")]
    public void Close()
    {
        isCloesd = true;
        UpdateDoor();
    }

    private void UpdateDoor()
    {
        if (other)
        {
            door.SetActive(isCloesd);
        }
    }

    [ContextMenu("Enter")]
    private void Enter() => GameManager.SetActiveRoom(this);
}
