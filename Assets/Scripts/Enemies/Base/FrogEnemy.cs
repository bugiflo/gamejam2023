using System;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class FrogEnemy : Enemy, IDamagable
{
    public int hp;
    public int damage;
    public float bulletSpeed;
    public Collider spawnArea;
    public GameObject bullet;
    public Animator animator;
    public GameObject avatarPivot;
    public GameObject dropShadow;
    public AnimationCurve jumpCurve;
    public int numberOfAttacks;
    public float maxJumpDistance;

    private int _currentHp;
    private NavMeshAgent _agent;
    private Transform _player;
    private FrogMovementSM _sm;
    private Collider _col;

    public event Action OnDeath;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _currentHp = hp;
        _col = GetComponent<Collider>();
    }

    private void Start()
    {
        _sm = transform.AddComponent<FrogMovementSM>();
        
        var bulletComponent = bullet.GetComponent<FrogBullet>();
        bulletComponent.Init(damage, bulletSpeed);
        
        _sm.InitFrogMovementSM(_agent, spawnArea, _player, bullet, animator, jumpCurve, avatarPivot, dropShadow, numberOfAttacks, _col, maxJumpDistance);
        
        _sm.RunStateMachine();
    }

    private void Update()
    {
        if (_player == null) _player = FindPlayer();

        var targetDirection = _player.position - transform.position;
        targetDirection = targetDirection.normalized;
        animator.SetFloat("Horizontal", Mathf.Round(targetDirection.x));
        animator.SetFloat("Vertical", Mathf.Round(targetDirection.z));

    }
    
    public Transform FindPlayer()
    {
        var player = GameObject.FindGameObjectWithTag("Player");

        _sm.Player = player.transform;

        return player != null ? player.transform : null;
    }

    public TakenDamage TakeDamage(int amount)
    {
        TakenDamage takenDamage;
        if (_currentHp <= amount)
        {
            takenDamage = new TakenDamage(_currentHp, true);
            Die();
        }
        else
        {
            takenDamage = new TakenDamage(amount, false);
            _currentHp -= amount;
        }

        return takenDamage;
    }

    public void Die()
    {
        OnDeath?.Invoke();
        Destroy(gameObject);
    }
    
}