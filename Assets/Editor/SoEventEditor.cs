﻿using UnityEditor;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CustomEditor(typeof(SoEvent))]
    public class SoEventEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        
            SoEvent myScriptableObject = (SoEvent)target;

            if (GUILayout.Button("Raise"))
            {
                myScriptableObject.Raise();
            }
        }
    }
}