﻿using UnityEngine;
using UnityEngine.Events;

namespace CowChow.GameJam2023
{
    public class PlayerDamageManager : MonoBehaviour, IDamagable
    {
        public Health Health;

        public SoEvent OnDeath;
        
        public TakenDamage TakeDamage(int amount)
        {
            TakenDamage takenDamage;
            if (Health.Current <= amount)
            {
                takenDamage = new TakenDamage(Health.Current, true);
                OnDeath.Raise();
            }
            else
            {
                takenDamage = new TakenDamage(amount, false);
                Health.Substract(amount);
            }

            return takenDamage;
        }
    }
}