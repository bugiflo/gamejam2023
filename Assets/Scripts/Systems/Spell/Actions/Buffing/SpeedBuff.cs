﻿using CowChow.GameJam2023.Buffing;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CreateAssetMenu(fileName = "Speed Buff", menuName = "Spell System/Actions/Buffs/Speed", order = 0)]
    public class SpeedBuff : LocalAction, IProjectileBuff
    {
        [SerializeField] private float launchSpeed = 60;
        
        public void ApplyToProjectile(IProjectile projectile)
        {
            projectile.LaunchForce = launchSpeed;
        }
    }
}