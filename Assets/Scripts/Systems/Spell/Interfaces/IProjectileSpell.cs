﻿using System.Collections.Generic;
using CowChow.GameJam2023.Buffing;
using UnityEngine;

namespace CowChow.GameJam2023
{
    public interface IProjectileSpell: IPhysicalSpell
    {
        IProjectile Projectile { get; set; }
        
        float InitialForce { get; set; }
        
        List<IProjectileBuff> Buffs { get; set; }
    }
}