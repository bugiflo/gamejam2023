public interface IDamagable
{
    /// <summary>
    /// Deals damage to the object.
    /// </summary>
    /// <param name="amount">The amount damage to apply to the object.</param>
    /// <returns>The effect on the object.</returns>
    TakenDamage TakeDamage(int amount);
}
