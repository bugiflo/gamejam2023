﻿using CowChow.GameJam2023.Utilities;
using UnityEngine;

namespace CowChow.GameJam2023
{
    public class LocalAction : GameAction, IPhysicalSpell
    {
        public override void Activate()
        {
            SpawnEffect();
        }

        private void SpawnEffect()
        {
            if (Effect && Origin)
            {
                GameObject newEffect = Instantiate(Effect, Origin.position, Origin.rotation);
                newEffect
                    .AddComponent<DelayedSelfDestruct>()
                    .destroyDelay = effectDestructionDelay;
                newEffect.transform.Rotate(Effect.transform.rotation.eulerAngles);
            }
        }


        public Transform Origin { get; set; }
    }
}