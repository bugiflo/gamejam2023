using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class SwitchSceneOnTimelineEnd : MonoBehaviour
{
    public PlayableDirector director;
    public string sceneName;

    private void Awake()
    {
        director.stopped += OnTimelineEnded;
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    void OnTimelineEnded(PlayableDirector director)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Single);
    }
    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        director.Play();
    }

    private void OnDisable()
    {
        director.stopped -= OnTimelineEnded;
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }

}

