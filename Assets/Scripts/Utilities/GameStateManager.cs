﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace CowChow.GameJam2023.Utilities
{
    public class GameStateManager : MonoBehaviour
    {
        
        public string GameOverScene;
        public string WinScene;

        public string menuScene;
        
        public void GameOver()
        {
            SceneManager.LoadScene(GameOverScene);
        }
        
        public void OnWin()
        {
            SceneManager.LoadScene(WinScene);
        }

        public void LoadMenu()
        {
            SceneManager.LoadScene(menuScene);

        }
    }
}