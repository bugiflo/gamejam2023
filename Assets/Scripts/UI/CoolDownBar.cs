using UnityEngine;
using UnityEngine.UI;

public class CoolDownBar : MonoBehaviour
{
    public Image cooldownBarLeft;
    public Image cooldownBarRight;

    public void UpdateCooldown(float remainingCooldown, float totalCooldown)
    {
        float step = (remainingCooldown / totalCooldown) * 0.5f;
        cooldownBarLeft.fillAmount = step;
        cooldownBarRight.fillAmount = step;
    }
}
