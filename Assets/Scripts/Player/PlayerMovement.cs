using UnityEngine;

public sealed class PlayerMovement : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("Components")] // ---------------------------------------------------------------------
    [SerializeField, Tooltip("The controller to effect the player.")]
    private CharacterController body = default;

    [Header("Speed")] // --------------------------------------------------------------------------
    [SerializeField, Tooltip("The max speed the character can move.")]
    private float speed = 2f;

    [SerializeField, Tooltip("The speed gain per second.")]
    private float acceleration = 20f;

    [SerializeField, Tooltip("The initial speed when dashing.")]
    private float dashStrength = 20f;

    [SerializeField, Tooltip("The portion of the spped that will be applied as acceleration.")]
    private float settle = 0.1f;


    // --- | Properties | --------------------------------------------------------------------------------------------------

    private Vector2 velocity = Vector2.zero;
    private Vector2 targetVelocity = Vector2.zero;


    // --- | MonoBehaviour | -----------------------------------------------------------------------------------------------

    private void FixedUpdate()
    {
        velocity = Vector2.MoveTowards(velocity, targetVelocity, (velocity.sqrMagnitude * settle + acceleration) * Time.fixedDeltaTime);
        body.Move((new Vector3(velocity.x, 0f, velocity.y) + Physics.gravity) * Time.fixedDeltaTime);
    }


    // --- | Methods | -----------------------------------------------------------------------------------------------------


    /// <summary>
    /// Moves the character in a direction (Y-Axis mapped to move forward).
    /// </summary>
    /// <param name="direction">The direction 
    public void Move(Vector2 direction)
    {
        targetVelocity = direction * speed;
    }


    /// <summary>
    /// Moves the character in a direction.
    /// </summary>
    /// <param name="direction">The direction to moves.</param>
    public void Dash(Vector2 direction)
    {
        velocity = direction * dashStrength;
    }
}
