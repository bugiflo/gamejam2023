﻿namespace CowChow.GameJam2023.Buffing
{
    public interface IProjectileBuff
    {
        public void ApplyToProjectile(IProjectile projectile);
    }
}