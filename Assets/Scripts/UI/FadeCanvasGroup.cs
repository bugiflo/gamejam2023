using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeCanvasGroup : MonoBehaviour
{
    public Button btn;
    public CanvasGroup group;
    public float fadeDuration = 1;
    public AnimationCurve fadeCurve;

    private bool isFading = false;
    private float remainingDuration;

    private void Awake()
    {
        btn.onClick.AddListener(OnClosePanel);
        remainingDuration = fadeDuration;
    }

    void OnClosePanel()
    {
        isFading = true;
    }

    private void Update()
    {
        if (isFading)
        {
            remainingDuration -= Time.deltaTime;
            group.alpha = fadeCurve.Evaluate(remainingDuration);
        }
    }

    private void OnDisable()
    {
        btn.onClick.RemoveListener(OnClosePanel);
    }
}
