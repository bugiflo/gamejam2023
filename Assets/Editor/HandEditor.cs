﻿using UnityEditor;
using UnityEngine;

namespace CowChow.GameJam2023
{

    [CustomEditor(typeof(Hand))]
    public class HandEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            Hand hand = (Hand)target;
            
            
            
            if (GUILayout.Button("Draw Hand Cards"))
            {
                hand.InitHand();
            }
            
            GUILayout.Space(10); // add space before buttons

            GUILayout.BeginHorizontal(); // align buttons next to each other

            if (GUILayout.Button("First Spell"))
            {
                hand.CastSpell((int)SpellSlot.First);
            }

            if (GUILayout.Button("Second Spell"))
            {
                hand.CastSpell((int)SpellSlot.Second);
            }

            if (GUILayout.Button("Third Spell"))
            {
                hand.CastSpell((int)SpellSlot.Third);
            }

            if (GUILayout.Button("Fourth Spell"))
            {
                hand.CastSpell((int)SpellSlot.Fourth);
            }
            GUILayout.EndHorizontal();
        }
    }

}