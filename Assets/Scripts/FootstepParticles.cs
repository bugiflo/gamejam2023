using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepParticles : MonoBehaviour
{
    public GameObject footstepParticles;

    public void OnFootstep()
    {
        Instantiate(footstepParticles, transform.position, Quaternion.identity);
    }
}
