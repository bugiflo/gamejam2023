using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The exits in the room.")]
    private Exit[] exits = new Exit[0];

    [SerializeField, Tooltip("The enemies in the room.")]
    private List<Enemy> enemies = new List<Enemy>();

    [SerializeField, Tooltip("The spwndes in the scene.")]
    private EnemySpawner[] spawner = new EnemySpawner[0];


    // --- | MonoBehaviour | -----------------------------------------------------------------------------------------------

    private void OnEnable()
    {
        RegisterEnemyListeners();
        RegsiterExtits();
    }

    private void OnDisable()
    {
        UnregisterExits();
        UnregisterEnemyListeners();
    }


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    private void RegisterEnemyListeners()
    {
        foreach (var enemy in GetLivingEnemies())
        {
            enemy.OnDeath += UnregisterEnemy;
        }
    }

    private void UnregisterEnemyListeners()
    {
        foreach (var enemy in GetLivingEnemies())
        {
            enemy.OnDeath -= UnregisterEnemy;
        }
    }

    private void RegsiterExtits()
    {
        foreach (var exit in exits)
        {
            exit.OnLeave += LeaveRoom;
            if (enemies.Count > 0)
            {
                exit.Close();
            }
            else
            {
                exit.Open();
            }
        }
    }

    private void UnregisterExits()
    {
        foreach (var exit in exits)
        {
            exit.Close();
            exit.OnLeave -= LeaveRoom;
        }
    }

    private void UnregisterEnemy()
    {
        foreach (var enemy in GetLivingEnemies())
        {
            return;
        }
        foreach (var exit in exits)
        {
            exit.Open();
        }
    }

    private void LeaveRoom(Exit entry)
    {
        GameManager.SetActiveRoom(entry);
    }

    private IEnumerable<Enemy> GetLivingEnemies()
    {
        for (int i = enemies.Count - 1; i >= 0; i--)
        {
            if (enemies[i] != null)
            {
                enemies.RemoveAt(i);
            }
            else
            {
                yield return enemies[i];
            }
        }
    }
}
