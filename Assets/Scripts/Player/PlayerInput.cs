using CowChow.GameJam2023;

using UnityEngine;
using UnityEngine.Events;

public class PlayerInput : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("Refferences")] // --------------------------------------------------------------------
    [SerializeField, Tooltip("The players root transform.")]
    private Transform body = default;

    [Header("Movement")] // -----------------------------------------------------------------------
    [SerializeField, Tooltip("The name of the axis to move the player forward.")]
    private string forwadAxis = "Vertical";

    [SerializeField, Tooltip("The name of the axis to move the player to the side.")]
    private string sidewaysAxis = "Horizontal";

    [Space, SerializeField, Tooltip("Called every frame with the current input.")]
    private UnityEvent<Vector2> onMove = default;
    /// <summary>
    /// Called every frame with the current input.
    /// </summary>
    public event UnityAction<Vector2> OnMove
    {
        add => onMove.AddListener(value);
        remove => onMove.RemoveListener(value);
    }

    [Header("Dash")] // ---------------------------------------------------------------------------
    [SerializeField, Tooltip("The key to trigger the dash.")]
    private KeyCode dashTrigger = KeyCode.Space;
    [Space, SerializeField, Tooltip("Called when the dash was triggered.")]
    private UnityEvent<Vector2> onDash = default;
    /// <summary>
    /// Called when the dash was triggered.
    /// </summary>
    public event UnityAction<Vector2> OnDash
    {
        add => onDash.AddListener(value);
        remove => onDash.RemoveListener(value);
    }

    [Header("Cast")] // ---------------------------------------------------------------------------
    [SerializeField, Tooltip("The key to cast the selected spell.")]
    private KeyCode castKey = KeyCode.Mouse0;

    [SerializeField, Tooltip("The layers that can be targeted for aim.")]
    private LayerMask groundLayers = default;

    [Space, SerializeField, Tooltip("Called when a spell was cast.")]
    private UnityEvent<Vector2> onCast = default;
    /// <summary>
    /// Called when a spell was cast.
    /// </summary>
    public event UnityAction<Vector2> OnCast
    {
        add => onCast.AddListener(value);
        remove => onCast.RemoveListener(value);
    }


    [Header("Select")] // -------------------------------------------------------------------------
    [SerializeField, Tooltip("The name of the axis controlling the scrolling through the cards.")]
    private string scrollAxisName = "Mouse ScrollWheel";

    [SerializeField, Tooltip("Called when a new slot was selected.")]
    private UnityEvent<SpellSlot> onSelect = default;
    /// <summary>
    /// Called when a new slot was selected.
    /// </summary>
    public event UnityAction<SpellSlot> OnSelect
    {
        add => onSelect.AddListener(value);
        remove => onSelect.RemoveListener(value);
    }

    [SerializeField, Tooltip("Called when the next slot should be selected.")]
    private UnityEvent onSelectNext = default;
    /// <summary>
    /// Called when the next slot should be selected.
    /// </summary>
    public event UnityAction OnSelectNext
    {
        add => onSelectNext.AddListener(value);
        remove => onSelectNext.RemoveListener(value);
    }

    [SerializeField, Tooltip("Called when the previous slot should be selected.")]
    private UnityEvent onSelectPrev = default;
    /// <summary>
    /// Called when the previous slot should be selected.
    /// </summary>
    public event UnityAction OnSelectPrev
    {
        add => onSelectPrev.AddListener(value);
        remove => onSelectPrev.AddListener(value);
    }


    // --- | Properties | --------------------------------------------------------------------------------------------------

    private const KeyCode CAST_FIRST_KEY = KeyCode.Alpha1;
    private static readonly KeyCode CAST_LAST_KEY = CAST_FIRST_KEY + System.Enum.GetValues(typeof(SpellSlot)).Length;
    private Vector2 direction;
    private new Camera camera;


    // --- | MonoBehaviour | -----------------------------------------------------------------------------------------------

    private void Awake()
    {
        camera = Camera.main;
    }

    private void Update()
    {
        CheckMovement();
        CheckSpell();
    }


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    private void CheckMovement()
    {
        direction = new Vector2(Input.GetAxisRaw(sidewaysAxis), Input.GetAxisRaw(forwadAxis));
        onMove.Invoke(direction);
        if (Input.GetKeyDown(dashTrigger))
        {
            onDash.Invoke(direction);
        }
    }

    private void CheckSpell()
    {
        CheckQuickCast();
        CheckSelection();
        CheckNormalCast();
    }

    private void CheckNormalCast()
    {
        if (Input.GetKeyDown(castKey))
        {
            onCast?.Invoke(GetAim());
        }
    }

    private void CheckSelection()
    {
        float scroll = Input.GetAxisRaw(scrollAxisName);
        if (scroll != 0f)
        {
            (scroll < 0f ? onSelectNext : onSelectPrev).Invoke();
        }
    }

    private void CheckQuickCast()
    {
        for (KeyCode key = CAST_FIRST_KEY; key <= CAST_LAST_KEY; key++)
        {
            if (Input.GetKeyDown(key))
            {
                onSelect.Invoke(KeyCodeToSpellSlot(key));
                onCast.Invoke(GetAim());
            }
        }
    }

    private SpellSlot KeyCodeToSpellSlot(KeyCode key)
    {
        return (SpellSlot)(key - CAST_FIRST_KEY);
    }

    private Vector2 GetAim()
    {
        if (Physics.Raycast(camera.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, groundLayers))
        {
            return new Vector2(hit.point.x - body.position.x, hit.point.z - body.position.z).normalized;
        }
        return Vector2.zero;
    }
}
