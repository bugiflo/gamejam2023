﻿using UnityEngine;

namespace CowChow.GameJam2023
{
    public class SoEventCaller : MonoBehaviour
    {
        public SoEvent eventToRaise;

        public void RaiseEvent()
        {
            if(eventToRaise == null) return;
            Debug.Log($"Raise Event on {name} assigned ev is {eventToRaise.name}");

            eventToRaise.Raise();
        }
    }
}