using System.Collections;
using UnityEngine;

public class FireFrogNormalAttack : BaseState
{
    private readonly FireFrogMovementSM _sm;
    private const int NumberOfAttack = 6;
    private const float SpreadRadius = 40;

    public FireFrogNormalAttack(FireFrogMovementSM stateMachine) : base("NormalAttack", stateMachine)
    {
        _sm = stateMachine;
    }

    public override void Enter()
    {
        _sm.StartCoroutine(StartAttack());
    }

    private IEnumerator StartAttack()
    {
        _sm.Animator.SetTrigger("Idle");

        yield return new WaitForSeconds(0.5f);
        
        // Change facing direction
        var enemyTransform = _sm.transform;
        var position = _sm.Player.position;
        var targetDirection = position - enemyTransform.position;
        targetDirection.y = position.y;
        
        // Shoot
        _sm.Animator.SetTrigger("Attack");
        
        yield return new WaitForSeconds(0.5f);
        
        const float radiusIncrement = SpreadRadius / (NumberOfAttack - 1);
        const float initialRadiusOffset = SpreadRadius / 2;

        for (var i = 0; i < NumberOfAttack; i++)
        {
            var newDirection = Quaternion.Euler(0, -initialRadiusOffset + radiusIncrement * i, 0) * targetDirection;
            Shoot(newDirection);
        }
        
        yield return new WaitForSeconds(1f);

        _sm.Animator.SetTrigger("Idle");
        _sm.ChangeState(_sm.Move);
        yield return null;
    }
    
    private void Shoot(Vector3 direction)
    {
        var bulletObj = _sm.InstantiateObject(_sm.Bullet);
        
        bulletObj.transform.position = new Vector3(_sm.transform.position.x, _sm.transform.position.y - 1f, _sm.transform.position.z);
        bulletObj.transform.rotation = Quaternion.LookRotation(direction);
    }
}