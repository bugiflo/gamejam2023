﻿using UnityEngine;
using UnityEngine.Events;

namespace CowChow.GameJam2023
{
    public class IntEventListener : MonoBehaviour
    {
        public IntEvent Event;
        public UnityEvent<int> Response;

        private void OnEnable()
        { Event.RegisterListener(this); }

        private void OnDisable()
        { Event.UnregisterListener(this); }

        public void OnEventRaised(int amount)
        { Response.Invoke(amount); }   
    }
}