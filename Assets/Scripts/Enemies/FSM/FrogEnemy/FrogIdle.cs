using UnityEngine.AI;

public class FrogIdle : BaseState
{
    private FrogMovementSM _sm;
    private bool _hasEntered;
    
    public FrogIdle(FrogMovementSM stateMachine) : base("Idle", stateMachine)
    {
        _sm = stateMachine;
    }

    public override void Enter()
    {
        if (_hasEntered) return;

        _hasEntered = true;

        var initialPosition = _sm.transform.position;
        if (!_sm.SpawnArea.bounds.Contains(_sm.transform.position))
        {
            initialPosition = _sm.SpawnArea.bounds.center;
        }

        NavMesh.SamplePosition(initialPosition, out var hit, 100, NavMesh.AllAreas);

        _sm.transform.position = hit.position;
        _sm.StartHeight = hit.position.y;

        _sm.Animator.SetTrigger("Idle");
    }

    public override void UpdateLogic()
    {
        _sm.ChangeState(_sm.Move);
    }
}
