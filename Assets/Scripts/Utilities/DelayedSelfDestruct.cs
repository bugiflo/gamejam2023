﻿using UnityEngine;

namespace CowChow.GameJam2023.Utilities
{
    public class DelayedSelfDestruct : MonoBehaviour
    {
        public float destroyDelay = 5f; // time delay in seconds before destroying the object

        // Start is called before the first frame update
        void Start()
        {
            // Destroy the object after the predefined delay
            Destroy(gameObject, destroyDelay);
        }
    }
}