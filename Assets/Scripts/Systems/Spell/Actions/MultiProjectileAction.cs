﻿using System.Collections.Generic;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CreateAssetMenu(fileName = "Default Multi Projectile Action", menuName = "Spell System/Actions/Multi Projectile", order = 0)]
    public class MultiProjectileAction: ProjectileAction
    {
        
        [Range(1, 20)]
        [SerializeField] private int shotCount = 3;

        [Range(0.0f, 360.0f)]
        [SerializeField] private float spreadRadius = 30;

        protected override void Shoot()
        {
            float radiusIncrement = spreadRadius / (shotCount - 1);

            float initialRadiusOffset = spreadRadius / 2;
            
            for (int i = 0; i < shotCount; i++)
            {
                Vector3 direction = Quaternion.Euler(0, -initialRadiusOffset + radiusIncrement * i, 0) * origin.forward;

                if (CreatNewProjectile(out var shotProjectile)) return;
                
                SetupProjectile(shotProjectile, direction);
                
                shotProjectile.Shoot(direction);  
                
            }
            
            Buffs.Clear();
        }
        
    }
}