﻿using CowChow.GameJam2023.Buffing;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CreateAssetMenu(fileName = "Speed Buff", menuName = "Spell System/Actions/Buffs/Scale", order = 0)]
    public class ScaleAction : LocalAction, IProjectileBuff
    {
        [SerializeField] private float scaleFactor = 2;

        public void ApplyToProjectile(IProjectile projectile)
        {
            if (!projectile.Rb)
            {
                return;
            }
            projectile.Rb.transform.localScale *= scaleFactor;
        }
    }
}