using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour
{
    public enum MusicType { Menu, Fight, Calm };
    public AudioSource musicSource;
    public AudioClip menuMusic;
    public AudioClip fightMusic;
    public AudioClip calmMusic;

    public static MusicManager Instance { get; private set; }

    private void Awake()
    {
        Instance ??= this;
        DontDestroyOnLoad(gameObject);
        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }

    public void ChangeMusic(MusicType value)
    {
        switch (value)
        {
            case MusicType.Menu:
                musicSource.clip = menuMusic;
                musicSource.Play();
                break;

            case MusicType.Fight:
                musicSource.clip = fightMusic;
                musicSource.Play();
                break;

            case MusicType.Calm:
                musicSource.clip = calmMusic;
                musicSource.Play();
                break;
        }
    }

    void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if(scene.name == "_Intro")
        {
            ChangeMusic(MusicType.Menu);
        }
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }
}
