﻿using System.Collections.Generic;
using UnityEngine;

namespace CowChow.GameJam2023.Utilities
{
    public class Utils
    {
        public static T RandomElementFromArray<T>(T[] array)
        {
            int randomIndex = Random.Range(0, array.Length);
            return array[randomIndex];
        }
        
        public static T RandomElementFromList<T>(List<T> array)
        {
            int randomIndex = Random.Range(0, array.Count);
            return array[randomIndex];
        }
    }
}