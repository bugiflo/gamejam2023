using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Card Collection", menuName = "Collections/Card")]
public class CardCollection : ScriptableObject
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The cards in the game.")]
    private UICard[] cards = new UICard[0];


    // --- | Properties | --------------------------------------------------------------------------------------------------

    private Dictionary<Spell, UICard> cardsBySpell;


    // --- | Methods | -----------------------------------------------------------------------------------------------

    private Dictionary<Spell, UICard> CreateDictionary()
    {
        cardsBySpell = new Dictionary<Spell, UICard>();
        foreach (UICard card in cards)
        {
            cardsBySpell.Add(card.Spell, card);
        }
        return cardsBySpell;
    }


    // --- | Operators | ---------------------------------------------------------------------------------------------------

    public UICard this[Spell spell] => (cardsBySpell ??= CreateDictionary())[spell];
}
