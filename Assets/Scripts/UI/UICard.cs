using CowChow.GameJam2023;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;

public class UICard : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The spell represented by this card.")]
    private Spell spell = default;
    /// <summary>
    /// The spell represented by this card.
    /// </summary>
    public Spell Spell => spell;

    [SerializeField, Tooltip("The cooldown of the spell.")]
    private CoolDownBar cooldown = default;


    // --- | Properties | --------------------------------------------------------------------------------------------------

    /// <summary>
    /// The transform component of the slot.
    /// </summary>
    public RectTransform RectTransform => (RectTransform)transform;


    // --- | MonoBehaviour | -----------------------------------------------------------------------------------------------

    private void OnEnable()
    {
        Spell.OnCast += StartCooldownUpdate;
    }
    private void OnDisable()
    {
        Spell.OnCast -= StartCooldownUpdate;
    }

    private void StartCooldownUpdate()
    {
        StartCoroutine(UpdateCooldown());
    }

    private IEnumerator UpdateCooldown()
    {
        float current;
        while (true)
        {
            current = spell.GetRemainingCooldown();
            if (current <= 0)
            {
                break;
            }
            cooldown.UpdateCooldown(current, spell.cooldown);
            yield return null;
        }
        cooldown.UpdateCooldown(0, spell.cooldown);
    }
}
