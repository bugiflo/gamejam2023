﻿using UnityEditor;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CustomEditor(typeof(Deck))]
    public class DeckEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            Deck deck = (Deck)target;

            GUILayout.Space(10); // add space before buttons

            GUILayout.BeginHorizontal(); // align buttons next to each other

            if (GUILayout.Button("Populate from source Deck"))
            {
                deck.PopulateFromSource();
                EditorUtility.SetDirty(deck);
            }
        
            if (GUILayout.Button("Populate from all Spells"))
            {
                deck.PopulateFromAllSpells();
                EditorUtility.SetDirty(deck);
            }

            GUILayout.EndHorizontal();
        }
    }
}