using System;
using System.Collections;
using System.Collections.Generic;
using CowChow.GameJam2023;
using Unity.VisualScripting;
using UnityEngine;

[CreateAssetMenu(fileName = "Default Spell", menuName = "Spell System/Spell", order = 0)]
public class Spell : ScriptableObject
{

    public float cooldown = 0.5f;

    private float cooldownFinished = 0;

    public GameAction action;

    public SpellEvent SpellEvent;

    public event UnityEngine.Events.UnityAction OnCast;

    private void OnEnable()
    {
        cooldownFinished = 0;
    }

    public void ResetCooldown()
    {
        cooldownFinished = 0;
    }
    
    /// <summary>
    /// Checks whether or not the spell can be cast at the current time.
    /// </summary>
    /// <returns></returns>
    public bool CanBeCast()
    {
        return Time.time > cooldownFinished;
    }

    /// <summary>
    /// Attempt to cast the spell
    /// </summary>
    /// <returns> Returns true on successful cast. </returns>
    public bool Cast()
    {
        if (!CanBeCast())
        {
            return false;
        }

        if (action == null)
        {
            Debug.LogError($"Couldn't cast spell ({this}), the Action was missing.");
            return false;
        }
        UpdateCooldown();
        
        SpellEvent.Raise(action);
        OnCast?.Invoke();
        return true;
    }

    private void UpdateCooldown()
    {
        cooldownFinished = Time.time + cooldown;
    }

    public float GetRemainingCooldown()
    {
        return cooldownFinished - Time.time;
    }
}

