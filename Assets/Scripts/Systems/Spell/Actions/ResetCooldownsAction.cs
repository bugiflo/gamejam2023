﻿using System;
using System.Collections.Generic;
using CowChow.GameJam2023.Utilities;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CreateAssetMenu(fileName = "Reset Cooldowns Action", menuName = "Spell System/Actions/Rest Cooldown", order = 0)]
    public class ResetCooldownsAction: LocalAction
    {
        [SerializeField] private List<Spell> possibleSpells;

        private void OnEnable()
        {
            possibleSpells = ScriptableObjectFinder.FindObjectsImplementingScript<Spell>();
        }


        public override void Activate()
        {
            foreach (Spell spell in possibleSpells)
            {
                spell.ResetCooldown();
            }
            base.Activate();
        }
    }
}