﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CreateAssetMenu(fileName = "Spell Event", menuName = "Event System/Spell Event", order = 0)]
    public class SpellEvent : ScriptableObject
    {
        private List<SpellEventListener> listeners = 
            new List<SpellEventListener>();

        public void Raise(GameAction action)
        {
            for(int i = listeners.Count -1; i >= 0; i--)
                listeners[i].OnEventRaised(action);
        }

        public void RegisterListener(SpellEventListener listener)
        { listeners.Add(listener); }

        public void UnregisterListener(SpellEventListener listener)
        { listeners.Remove(listener); }

        internal void RegisterListener(object startUpdate)
        {
            throw new NotImplementedException();
        }
    }
}