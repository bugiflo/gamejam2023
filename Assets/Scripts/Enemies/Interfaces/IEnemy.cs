using UnityEngine;

public interface IEnemy
{
    public event System.Action OnDeath;
}