﻿using UnityEngine;
using UnityEngine.Events;

namespace CowChow.GameJam2023
{
    public class SoEventListener : MonoBehaviour
    {
        
        public SoEvent Event;
        public UnityEvent Response;

        private void OnEnable()
        { Event.RegisterListener(this); }

        private void OnDisable()
        { Event.UnregisterListener(this); }

        public void OnEventRaised()
        { Response.Invoke(); }   
    }
}