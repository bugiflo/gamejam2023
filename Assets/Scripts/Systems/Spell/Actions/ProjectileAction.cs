﻿using System;
using System.Collections.Generic;
using CowChow.GameJam2023.Buffing;
using Unity.VisualScripting;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CreateAssetMenu(fileName = "Default Projectile Action", menuName = "Spell System/Actions/Single Projectile", order = 0)]
    public class ProjectileAction : GameAction, IProjectileSpell
    {
        public Rigidbody projectileObject;

        private IProjectile _projectile;
        
        public IProjectile Projectile { 
            get => _projectile;
            set => _projectile = value;
        }

        public float launchForce = 40;
        public float InitialForce {
            get => launchForce;
            set => launchForce = value;
        }


        private List<IProjectileBuff> _buffs = new ();
        public List<IProjectileBuff> Buffs { get => _buffs; set => _buffs = value; }

        public Transform origin;
        public Transform Origin {
            get => origin;
            set => origin = value;
        }
        
        public override void Activate()
        {
            if (CheckObjects()) return;

            Shoot();
        }

        protected virtual void Shoot()
        {
            if (CreatNewProjectile(out var shotProjectile)) return;
            SetupProjectile(shotProjectile);

            shotProjectile.Shoot(origin.forward);
            
            Buffs.Clear();
        }

        protected bool CreatNewProjectile(out IProjectile shotProjectile)
        {
            Rigidbody newRB = Instantiate(Projectile.Rb);

            shotProjectile = newRB.GetComponent<IProjectile>();

            if (shotProjectile == null)
            {
                Debug.LogError($"{newRB} is missing it's IProjectile Script");
                return true;
            }

            return false;
        }

        private bool CheckObjects()
        {
            if (Projectile == null || !Projectile.Rb || !origin)
            {
                Debug.LogWarning($"Couldn't perform Action {this}, Projectile, Rigidbody or origin was missing");
                return true;
            }

            return false;
        }
        

        protected void SetupProjectile(IProjectile shotProjectile)
        {
            SetupProjectile(shotProjectile, origin.forward);
        }
        
        protected void SetupProjectile(IProjectile shotProjectile, Vector3 direction )
        {
            shotProjectile.Rb.velocity = Vector3.zero;
            shotProjectile.Rb.transform.SetPositionAndRotation(origin.position, Quaternion.LookRotation(direction));
            shotProjectile.LaunchForce = launchForce;
            if (Effect)
            {
                ProjectileEffect projectileEffect = shotProjectile.Rb.AddComponent<ProjectileEffect>();
                projectileEffect.Effect = Effect;
                projectileEffect.effectDestructionDelay = effectDestructionDelay;
            }
            ApplyBuffs(shotProjectile);
        }

        private void ApplyBuffs(IProjectile shotProjectile)
        {
            foreach (IProjectileBuff buff in Buffs)
            {
                buff.ApplyToProjectile(shotProjectile);
            }
        }
        

        private void OnValidate()
        {
            ValidateProjectileRigidbody();
        }

        private void ValidateProjectileRigidbody()
        {
            if (!projectileObject)
            {
                return;
            }
            if (projectileObject.GetComponent<IProjectile>() == null)
            {
                projectileObject = null;
                Debug.LogError($"Selected Rigidbody doesn't Implement the {typeof(IProjectile)} interface," +
                               $"please check the Object.");
                return;
            }

            _projectile = projectileObject.GetComponent<IProjectile>();
            _projectile.SetUpProjectileRb();
        }
    }
}