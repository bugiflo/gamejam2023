using UnityEngine;
using UnityEngine.Events;

public class Health : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The max health.")]
    private int max = 5;
    /// <summary>
    /// The max health.
    /// </summary>
    public int Max => max;

    [Space, SerializeField, Tooltip("Called when the health changes.")]
    private UnityEvent<int, int> onUpdate = default;
    /// <summary>
    /// Called when the health changes.
    /// </summary>
    public event UnityAction<int, int> OnUpdate
    {
        add => onUpdate.AddListener(value);
        remove => onUpdate.RemoveListener(value);
    }


    // --- | Properties | --------------------------------------------------------------------------------------------------

    /// <summary>
    /// The current amount of health.
    /// </summary>
    public int Current { get; private set; }


    // --- | MonoBehaviour | -----------------------------------------------------------------------------------------------

    private void Awake()
    {
        Current = max;
    }


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    public void Set(int amount)
    {
        Current = amount;
        onUpdate.Invoke(Current, max);
    }

    public void Add(int amount) => Set(Current + amount);
    public void Substract(int amount) => Set(Current-+ amount);


}
