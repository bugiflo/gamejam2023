﻿using UnityEngine;

namespace CowChow.GameJam2023
{
    [CreateAssetMenu(fileName = "Default Projectile Action", menuName = "Spell System/Actions/Heal", order = 0)]
    public class HealingAction : LocalAction
    {
        [SerializeField] public int HealAmount = 2;
        [SerializeField] public IntEvent healEvent;

        public override void Activate()
        {
            healEvent.Raise(HealAmount);
            base.Activate();
        }
    }
}