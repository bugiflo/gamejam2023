using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class BasicEnemy : Enemy, IDamagable
{
    public int hp;
    public int damage;
    public float damageDistanceThreshold = 1f;
    public Animator animator;
    public GameObject deathParticles;
    
    private int _currentHp;
    private NavMeshAgent _agent;
    private Transform _player;
    private bool _canMove;

    public event Action OnDeath;

    private void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _currentHp = hp;
    }

    private void Start()
    {
        _player = FindPlayer();
        var nextMovementPosition = FindNewPosition();
        StartCoroutine(StartMoveRoutine(nextMovementPosition));
    }

    private void Update()
    {
        if (_player == null) _player = FindPlayer();

        var nextMovementPosition = FindNewPosition();
        Move(nextMovementPosition);
        CheckForPlayerDistance(_player.position);

        var targetDirection = _player.position - transform.position;
        targetDirection = targetDirection.normalized;
        animator.SetFloat("Horizontal", Mathf.Round(targetDirection.x));
        animator.SetFloat("Vertical", Mathf.Round(targetDirection.z));
    }

    public Vector3 FindNewPosition()
    {
        NavMesh.SamplePosition(_player.position, out var hit, 100, NavMesh.AllAreas);
        return hit.position;
    }

    public void Move(Vector3 position)
    {
        if (_canMove) _agent.destination = position;

    }

    public void CheckForPlayerDistance(Vector3 playerPosition)
    {
        var distance = Vector3.Distance(transform.position, playerPosition);
        if (distance > damageDistanceThreshold) return;

        var playerDamageComponent = _player.GetComponent<IDamagable>();
        playerDamageComponent?.TakeDamage(damage);

        Die();
    }

    public Transform FindPlayer()
    {
        var player = GameObject.FindGameObjectWithTag("Player");

        return player != null ? player.transform : null;
    }

    public TakenDamage TakeDamage(int amount)
    {
        TakenDamage takenDamage;
        if (_currentHp <= amount)
        {
            takenDamage = new TakenDamage(_currentHp, true);
            Die();
        }
        else
        {
            takenDamage = new TakenDamage(amount, false);
            _currentHp -= amount;
        }

        return takenDamage;
    }

    public void Die()
    {
        Instantiate(deathParticles, transform.position, Quaternion.identity);
        OnDeath?.Invoke();
        Destroy(gameObject);
    }

    private IEnumerator StartMoveRoutine(Vector3 position)
    {
        animator.SetTrigger("Idle");
        yield return new WaitForSeconds(1f);

        animator.SetTrigger("Walk");
        _canMove = true;
    }
}