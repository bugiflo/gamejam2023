using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class FireFrogSpecialAttack : BaseState
{
    private readonly FireFrogMovementSM _sm;
    private int _currentJump;
    private readonly float _oldSpeed;
    private readonly Vector3 _startScale;
    private float _travelDistance;
    
    public FireFrogSpecialAttack(FireFrogMovementSM stateMachine) : base("SpecialAttack", stateMachine)
    {
        _sm = stateMachine;
        
        _oldSpeed = _sm.Agent.speed;
        _startScale = _sm.AvatarPivot.transform.localScale;
    }

    public override void Enter()
    {
        _currentJump = 0;
        _sm.Agent.speed = _sm.SpecialAttackSpeed;

        _sm.StartCoroutine(SpecialAttackRoutine());
    }

    private IEnumerator SpecialAttackRoutine()
    {
        _currentJump++;
        _sm.Animator.SetTrigger("Idle");
        yield return new WaitForSeconds(0.5f);

        var newJumpPosition = FindNewPosition();
        _sm.Agent.SetDestination(newJumpPosition);
        
        yield return new WaitWhile(() => _sm.Agent.pathPending);
        
        var travelDistance = _sm.Agent.remainingDistance;
        
        // Set speed according to distance.
        var speedAddition = Mathf.Lerp(1, 2, travelDistance);
        _sm.Agent.speed = _oldSpeed * speedAddition;
        
        _sm.Animator.SetTrigger("Jump");
        _sm.Col.enabled = false;
        
        var agent = _sm.Agent;
        yield return new WaitUntil(() =>
        {
            var remainingDistance = agent.remainingDistance;

            var evaluatedCurve = _sm.SpecialAttackCurve.Evaluate(1f - (remainingDistance / travelDistance)) * 2;

            var position = _sm.transform.position;
            var newPivotPosition = new Vector3(position.x, _sm.StartHeight + evaluatedCurve,
                position.z);
            var newShadowScale = new Vector3(_startScale.x * (1 + evaluatedCurve), _startScale.y * (1 + evaluatedCurve),
                _startScale.z * (1 + evaluatedCurve));

            _sm.AvatarPivot.transform.position = newPivotPosition;
            _sm.DropShadow.transform.localScale = newShadowScale;

            return !float.IsInfinity(remainingDistance) && agent.pathStatus == NavMeshPathStatus.PathComplete &&
                   agent.remainingDistance == 0;
        });
        
        _sm.Col.enabled = true;
        _sm.Animator.SetTrigger("Idle");
        
        yield return new WaitForSeconds(0.2f);
        SpecialAttack();
        
        yield return new WaitForSeconds(1f);
        
        if (_currentJump < _sm.SpecialAttackAmount)
        {
            
            _sm.StartCoroutine(SpecialAttackRoutine());
        }
        else
        {
            _sm.Agent.speed = _oldSpeed;
            _sm.ChangeState(_sm.Move);
        }
        
    }

    private Vector3 FindNewPosition()
    {
        var randomPosition = _sm.transform.position + Random.insideUnitSphere * _sm.SpecialAttackRadius;
        NavMesh.SamplePosition(randomPosition, out var hit, _sm.SpecialAttackRadius, NavMesh.AllAreas);
    
        return hit.position;
    }

    private void SpecialAttack()
    {
        _sm.Nova.Clear();
        _sm.Nova.Play();
        
        if (_sm.Source != null) _sm.Source.Play();
    }
}