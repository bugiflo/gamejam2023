using UnityEngine;

public abstract class Enemy : MonoBehaviour
{
    public event System.Action OnDeath;
}