using CowChow.GameJam2023;
using System;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [Header("UIs")] // ----------------------------------------------------------------------------
    [SerializeField]
    private GameObject ui3D = default;

    [SerializeField]
    private PlayerController player = default;

    [Header("Components")] // ---------------------------------------------------------------------
    [SerializeField]
    private UIDrawnCards handDisplay = default;

    [SerializeField]
    private NumericHealthbar healthbar = default;

    private void Awake()
    {
        healthbar.SetHealth(player.Health);
    }

    private void OnEnable()
    {
        handDisplay.BeforeUpdate += Refresh3DUI;
    }

    private void OnDisable()
    {
        handDisplay.BeforeUpdate -= Refresh3DUI;
    }

    private void LateUpdate()
    {
        if (!ui3D.activeSelf)
        {
            ui3D.SetActive(true);
        }
    }

    public void Refresh3DUI()
    {
        ui3D.SetActive(false);
    }

    public void HighlightSlot(SpellSlot slot) => handDisplay.HighlightSpell(slot);
}
