using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrogNova : MonoBehaviour
{
    [SerializeField] private int _damage;
    [SerializeField] private float _damageCooldown;

    private bool isOnCD = false;

    void OnParticleCollision(GameObject other)
    {
        if (other.CompareTag("Player"))
        {
            if (!isOnCD)
            {
                StartCoroutine(damageCooldown(other));
            }

        }
    }

    private IEnumerator damageCooldown(GameObject other)
    {
        isOnCD = true;
        var playerDamageComponent = other.GetComponent<IDamagable>();
        playerDamageComponent?.TakeDamage(_damage);
        yield return new WaitForSeconds(_damageCooldown);
        isOnCD = false;
    }
}
