using System.Collections;
using UnityEngine;

public class FrogAttack : BaseState
{
    private readonly FrogMovementSM _sm;
    
    public FrogAttack(FrogMovementSM stateMachine) : base("Attack", stateMachine)
    {
        _sm = stateMachine;
    }

    public override void Enter()
    {
        base.Enter();
        _sm.StartCoroutine(ChangeState());
    }
    
    private IEnumerator ChangeState()
    {
        _sm.Animator.SetTrigger("Idle");

        yield return new WaitForSeconds(0.5f);
        
        // Change facing direction
        var enemyTransform = _sm.transform;
        var position = _sm.Player.position;
        var targetDirection = position - enemyTransform.position;
        targetDirection.y = position.y;
        //enemyTransform.rotation = Quaternion.LookRotation(targetDirection);

        // Shoot
        _sm.Animator.SetTrigger("Attack");

        for (var i = 0; i < _sm.NumberOfAttacks; i++)
        {
            yield return new WaitForSeconds(0.5f);
            Shoot(targetDirection);
        }

        yield return new WaitForSeconds(0.5f);

        _sm.Animator.SetTrigger("Idle");
        _sm.ChangeState(_sm.Move);
        yield return null;
    }

    private void Shoot(Vector3 direction)
    {
        var bulletObj = _sm.InstantiateObject(_sm.Bullet);
        
        bulletObj.transform.position = _sm.transform.position;
        bulletObj.transform.rotation = Quaternion.LookRotation(direction);
    }
}