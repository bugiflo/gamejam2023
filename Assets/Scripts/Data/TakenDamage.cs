public sealed class TakenDamage
{
    // --- | Properties | --------------------------------------------------------------------------------------------------

    /// <summary>
    /// The damage dealt to the object.
    /// </summary>
    public int DamageDealt { get; private set; }

    /// <summary>
    /// True if the object was killed/destroyed.
    /// </summary>
    public bool WasDestroyed { get; private set; }


    // --- | Constructors | ------------------------------------------------------------------------------------------------

    public TakenDamage(int damageTaken, bool wasDestroyed)
    {
        DamageDealt = damageTaken;
        WasDestroyed = wasDestroyed;
    }
}
