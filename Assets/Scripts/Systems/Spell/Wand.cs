﻿using System.Collections.Generic;
using CowChow.GameJam2023.Buffing;
using UnityEngine;

namespace CowChow.GameJam2023
{
    public class Wand : MonoBehaviour
    {
        [SerializeField] private Transform selfEffectSpawner;
        
        private Queue<IProjectileBuff> ProjectileBuffs = new Queue<IProjectileBuff>();

        public void InvokeSpell(GameAction action)
        {
            HandleInWorldSpell(action);

            HandleProjectileBuffs(action);
            
            HandleProjectileAction(action);

            action.Activate();
        }

        private void HandleInWorldSpell(GameAction action)
        {
            if (!(action is IPhysicalSpell))
            {
                return;
            }

            if (action is LocalAction && selfEffectSpawner)
            {
                (action as IPhysicalSpell).Origin = selfEffectSpawner;
                return;
            }
            
            (action as IPhysicalSpell).Origin = this.transform;

        }
        
        private void HandleProjectileBuffs(GameAction action)
        {
            if (action is IProjectileBuff)
            {
                ProjectileBuffs.Enqueue(action as IProjectileBuff);
            }
        }
        
        private void HandleProjectileAction(GameAction action)
        {
            if (action is ProjectileAction)
            {
                if(ProjectileBuffs != null && ProjectileBuffs.Count > 0)
                {
                    List<IProjectileBuff> buffList = new List<IProjectileBuff>();
                    for (int i = 0; i < ProjectileBuffs.Count; i++)
                    {
                        buffList.Add(ProjectileBuffs.Dequeue());
                    }
                    (action as ProjectileAction).Buffs = buffList;
                }
            }
        }
    }
}