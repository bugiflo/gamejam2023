using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDummy : MonoBehaviour, IDamagable
{
    public int Health = 1;

    [SerializeField] private bool destroyed = false;
    
    public TakenDamage TakeDamage(int amount)
    {
        int newHealth = Health - amount;

        if (newHealth <= 0)
        {
            destroyed = true;
            newHealth = 0;
        }

        int diff = Health - newHealth;
        Health = newHealth;

        return new TakenDamage(diff, true);
    }

    private void Update()
    {
        if (destroyed)
        {
            StartDisableCoroutine();
            destroyed = false;
        }
    }
    
    private bool isDisabled = false;

    // Call this method to start the coroutine
    public void StartDisableCoroutine()
    {
        if (!isDisabled)
        {
            StartCoroutine(DisableCoroutine());
        }
    }

    private IEnumerator DisableCoroutine()
    {
        isDisabled = true;
        GetComponent<Collider>().enabled = false;
        GetComponent<Renderer>().enabled = false;
        
        yield return new WaitForSeconds(3f);

        GetComponent<Collider>().enabled = true;
        GetComponent<Renderer>().enabled = true;
        isDisabled = false;
    }
}
