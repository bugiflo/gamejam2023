using UnityEngine;
using UnityEngine.AI;

public class EnemyFactory : MonoBehaviour
{
    public static EnemyFactory Instance { get; private set; }

    private void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this);
        }
        else
        {
            Instance = this;
        }
    }
    
    public GameObject basicEnemyObject;

    public GameObject CreateBasicEnemy(Vector3 spawnPosition)
    {
        NavMesh.SamplePosition(spawnPosition, out var hit, 100, 1);
        var navmeshPosition = hit.position;
        var newBasicEnemyObject = Instantiate(basicEnemyObject);
        newBasicEnemyObject.transform.position = navmeshPosition;
        
        return newBasicEnemyObject;
    }
}