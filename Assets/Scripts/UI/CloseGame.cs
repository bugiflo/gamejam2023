using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloseGame : MonoBehaviour
{
    public Button btn;

    private void Awake()
    {
        btn.onClick.AddListener(OnButtonPress);
    }

    void OnButtonPress()
    {
        Application.Quit();
    }

    private void OnDisable()
    {
        btn.onClick.RemoveListener(OnButtonPress);
    }
}
