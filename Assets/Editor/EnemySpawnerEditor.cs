using UnityEditor;

namespace Editor
{
    [CustomEditor(typeof(EnemySpawner))]
    public class EnemySpawnerEditor : UnityEditor.Editor
    {
        private SerializedProperty _enemyTypeProp;
        private SerializedProperty _spawnDelayProp;
        private SerializedProperty _useSpawnRadiusProp;
        private SerializedProperty _spawnRadiusProp;
        private SerializedProperty _spawnSingleEnemyProp;
        private SerializedProperty _enemyAmountProp;

        void OnEnable()
        {
            _enemyTypeProp = serializedObject.FindProperty("enemyType");
            _spawnDelayProp = serializedObject.FindProperty("spawnDelay");
            _useSpawnRadiusProp = serializedObject.FindProperty("useSpawnRadius");
            _spawnRadiusProp = serializedObject.FindProperty("spawnRadius");
            _spawnSingleEnemyProp = serializedObject.FindProperty("spawnSingleEnemy");
            _enemyAmountProp = serializedObject.FindProperty("enemyAmount");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(_enemyTypeProp);
            EditorGUILayout.PropertyField(_spawnDelayProp);

            EditorGUILayout.PropertyField(_useSpawnRadiusProp);

            if (_useSpawnRadiusProp.boolValue)
            {
                EditorGUILayout.PropertyField(_spawnRadiusProp);
            }

            EditorGUILayout.PropertyField(_spawnSingleEnemyProp);

            if (!_spawnSingleEnemyProp.boolValue)
            {
                EditorGUILayout.PropertyField(_enemyAmountProp);
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}