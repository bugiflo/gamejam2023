using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SoundCollection", menuName = "Collections/Sound")]
public class SoundCollection : ScriptableObject, IEnumerable<AudioClip>
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [SerializeField, Tooltip("The collection of clips.")]
    private AudioClip[] clips = new AudioClip[0];

    /// <summary>
    /// The amount of clips in the collection.
    /// </summary>
    public int Count => clips.Length;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Returns a random clip from the collection.
    /// </summary>
    /// <returns>The random clip.</returns>
    public AudioClip GetRandom()
    {
        return clips[Random.Range(0, clips.Length)];
    }

    public IEnumerator<AudioClip> GetEnumerator()
    {
        foreach (AudioClip clip in clips)
        {
            yield return clip;
        }
    }

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();


    // --- | Operators | ---------------------------------------------------------------------------------------------------

    public AudioClip this[int key] => clips[key];
}
