using UnityEngine;

public class PlayerAnimator : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("Components")] // ---------------------------------------------------------------------
    [SerializeField, Tooltip("The players animator.")]
    private Animator animatior;

    [Header("Movement")] // -----------------------------------------------------------------------
    [SerializeField, Tooltip("The value set in the animator when moving for- & backward.")]
    private string sidewaysMovement = "Horizontal";

    [SerializeField, Tooltip("The value set in the animator when moving right and left.")]
    private string forwardMovement = "Vertical";

    [SerializeField, Tooltip("The absolute value that must be given to count as a movement input.")]
    private float movementSensitivity = 0.5f;


    [Header("Dash")] // ---------------------------------------------------------------------------
    [SerializeField, Tooltip("The name of the trigger in the animator.")]
    private string dashTrigger = "Dash";


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Updates the movement animation.
    /// </summary>
    /// <param name="direction">The direction the character is moving [-1;+1].</param>
    public void Move(Vector2 direction)
    {
        animatior.SetFloat(sidewaysMovement, ClampValueToSensitivity(direction.x));
        animatior.SetFloat(forwardMovement, ClampValueToSensitivity(direction.y));
    }

    /// <summary>
    /// Triggers the dash animation.
    /// </summary>
    /// <param name="direction"></>
    public void Dash()
    {
        animatior.SetTrigger(dashTrigger);
    }

    private float ClampValueToSensitivity(float value)
    {
        if (value > movementSensitivity)
        {
            return +1f;
        }
        else if (value < -movementSensitivity)
        {
            return -1f;
        }
        return 0;
    }
}
