﻿using UnityEditor;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CustomEditor(typeof(Spell))]
    public class SpellEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
        
            Spell myScriptableObject = (Spell)target;

            if (GUILayout.Button("Cast"))
            {
                myScriptableObject.Cast();
            }
            
            if (GUILayout.Button("Reset Cooldown"))
            {
                myScriptableObject.ResetCooldown();
            }
        }
    }
}