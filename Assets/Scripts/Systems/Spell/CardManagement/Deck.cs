﻿using System;
using System.Collections.Generic;
using System.Linq;
using CowChow.GameJam2023.Utilities;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace CowChow.GameJam2023
{
    [CreateAssetMenu(fileName = "Default Deck", menuName = "Spell System/Deck", order = 0)]
    public class Deck : ScriptableObject
    {
    
        [SerializeField] private int deckSize;
        [FormerlySerializedAs("availableCards")] [SerializeField] private List<Spell> deckCards;
        [SerializeField] private Deck sourceDeck;

        public List<Spell> CardsInDeck => deckCards;


        public Spell GetRandomSpell()
        {
            return Utils.RandomElementFromList(CardsInDeck);
        }

        public void PopulateFromSource()
        {
            if (!sourceDeck)
            {
                Debug.LogWarning($"{name} is missing a source deck, please add one to populate from there");
                return;
            }
            PopulateDeck(sourceDeck.CardsInDeck, deckSize);
        }
        
        public void PopulateFromAllSpells()
        {
            List<Spell> possibleSpells = ScriptableObjectFinder.FindObjectsImplementingScript<Spell>();
            int size = possibleSpells.Count;
            PopulateDeck(possibleSpells, size);
        }

        private void PopulateDeck(List<Spell> sourceCards, int size)
        {
            Debug.Log("ds");

            deckCards = new List<Spell>(size);

            for (int i = 0; i < size; i++)
            {
                CardsInDeck.Add(sourceCards[i % sourceCards.Count]);
            }    
        }

        public Spell DrawAndReplace(Spell oldSpell)
        {
            int drawIndex = Random.Range(0, CardsInDeck.Count);
            Spell drawnSpell = CardsInDeck[drawIndex];
            CardsInDeck[drawIndex] = oldSpell;
            return drawnSpell;
        }

        public Spell Draw()
        {
            Spell drawnCard = CardsInDeck.Last();
            CardsInDeck.RemoveAt(CardsInDeck.Count - 1);
            return drawnCard;
        }

        public void Replace(Spell newSpell, int oldSpellIndex)
        {
            CardsInDeck.RemoveAt(oldSpellIndex);
            CardsInDeck.Add(newSpell);
        }
    }
}