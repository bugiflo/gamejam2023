using System;
using UnityEngine;

namespace CowChow.GameJam2023
{
    public enum SpellSlot
    {
        First,
        Second,
        Third,
        Fourth
    }
    
    [CreateAssetMenu(fileName = "Hand", menuName = "Spell System/Hand", order = 0)]
    public class Hand : ScriptableObject
    {
        [SerializeField]
        private Spell[] availableSpells = new Spell[4];

        [SerializeField] private Deck _deck;

        [SerializeField] private IntEvent slotChanged;

        public Spell[] AvailableSpells => availableSpells;


        /// <summary>
        /// Attempts to cast the Spell at the Specified position.
        /// </summary>
        /// <param name="spellSlot"> Index of the spell, possible</param>
        /// <returns></returns>
        public bool CastSpell(int spellSlot)
        {
            Spell spellToCast = GetSpellAtSlot(spellSlot);
            if (!spellToCast)
            {
                return false;
            }

            if (!spellToCast.Cast())
            {
                return false;
            }

            RedrawCard(spellSlot);
            
            return true;
        }

        public void RedrawCard(int spellSlot)
        {
            ReplaceSpellAtSlot(Slot(spellSlot), _deck.DrawAndReplace(GetSpellAtSlot(spellSlot)));
        }

        public Spell GetSpellAtSlot(int spellSlot)
        {
            return AvailableSpells[Slot(spellSlot)];
        }

        private bool ReplaceSpellAtSlot(int spellSlot, Spell newSpell)
        {
            if (!newSpell) return false;
            AvailableSpells[Slot(spellSlot)] = newSpell;
            slotChanged.Raise(Slot(spellSlot));
            return true;
        }

        /// <summary>
        /// Checks the Remaining cooldown of all Spells and returns them in an array according to their spellslots
        /// </summary>
        /// <returns>Array of remaining Cooldowns</returns>
        public float[] GetCooldowns()
        {
            float[] cooldowns = new float[SlotsLength()];

            for (int i = 0; i < cooldowns.Length; i++)
            {
                cooldowns[i] = availableSpells[i].GetRemainingCooldown();
            }

            return cooldowns;
        }

        public void InitHand()
        {
            for (int index = 0; index < SlotsLength(); index++)
            {
                availableSpells[index] = _deck.Draw();
                slotChanged.Raise(Slot(index));

            }
        }

        public void RedrawHand()
        {
            for (int spellSlot = 0; spellSlot < SlotsLength(); spellSlot++)
            {
                RedrawCard(Slot(spellSlot));

            }
        }

        public int Slot(int va)
        {
            return va % SlotsLength();
        }

        public int SlotsLength()
        {
            return availableSpells.Length;
        }

    }
}