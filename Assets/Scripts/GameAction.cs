﻿using UnityEngine;

namespace CowChow.GameJam2023
{
    public abstract class GameAction : ScriptableObject
    {
        [SerializeField] protected GameObject Effect;
        [SerializeField] protected float effectDestructionDelay = 2;

        
        public abstract void Activate();
        
    }
}