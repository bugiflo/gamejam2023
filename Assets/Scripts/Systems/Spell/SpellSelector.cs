using UnityEngine;
using UnityEngine.Events;

namespace CowChow.GameJam2023
{
    public class SpellSelector : MonoBehaviour
    {
        // --- | Inspector | -----------------------------------------------------------------------------------------------

        [SerializeField, Tooltip("The currently selected slot.")]
        private SpellSlot selectedSlot = default;
        /// <summary>
        /// The currently selected slot.
        /// </summary>
        public SpellSlot Current => selectedSlot;

        [Space, SerializeField, Tooltip("Called when the selection was update.")]
        private UnityEvent<SpellSlot> onUpdate = default;
        /// <summary>
        /// Called when the selection was update.
        /// </summary>
        public event UnityAction<SpellSlot> OnUpdate
        {
            add => onUpdate.AddListener(value);
            remove => onUpdate.RemoveListener(value);
        }


        // --- | Properties | ----------------------------------------------------------------------------------------------

        private static readonly int SLOT_COUNT = System.Enum.GetValues(typeof(SpellSlot)).Length;


        // --- | Methods | -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Select a slot.
        /// </summary>
        /// <param name="slot">The slot to select.</param>
        public void Select(SpellSlot slot)
        {
            selectedSlot = slot;
            onUpdate?.Invoke(selectedSlot);
        }

        /// <summary>
        /// Selects the next slot.
        /// </summary>
        public void Next() => Select((SpellSlot)(((int)selectedSlot + 1) % SLOT_COUNT));
        /// <summary>
        /// Selects the previous slot.
        /// </summary>
        public void Prev() => Select((SpellSlot)(((int)selectedSlot + SLOT_COUNT - 1) % SLOT_COUNT));
    }
}
