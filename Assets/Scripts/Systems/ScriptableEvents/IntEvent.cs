﻿using System.Collections.Generic;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CreateAssetMenu(fileName = "Int Event", menuName = "Event System/Int Event", order = 0)]
    public class IntEvent : ScriptableObject
    {
        private List<IntEventListener> listeners = 
            new List<IntEventListener>();

        public void Raise(int amount)
        {
            for(int i = listeners.Count -1; i >= 0; i--)
                listeners[i].OnEventRaised(amount);
        }

        public void RegisterListener(IntEventListener listener)
        { listeners.Add(listener); }

        public void UnregisterListener(IntEventListener listener)
        { listeners.Remove(listener); }
    }
}