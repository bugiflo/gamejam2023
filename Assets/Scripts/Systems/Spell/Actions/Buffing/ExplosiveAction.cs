﻿using CowChow.GameJam2023.Behaviours;
using CowChow.GameJam2023.Buffing;
using Unity.VisualScripting;
using UnityEngine;

namespace CowChow.GameJam2023
{
    [CreateAssetMenu(fileName = "Explosive Action", menuName = "Spell System/Actions/Buffs/Explosive", order = 0)]
    public class ExplosiveAction : LocalAction, IProjectileBuff
    {
        
        [SerializeField] private float explosionRadius;
        
        [SerializeField] private int explosionDamage;

        [SerializeField] private GameObject explosion;

        public void ApplyToProjectile(IProjectile projectile)
        {
            if (!projectile.Rb)
            {
                return;
            }

            Explosive explosive = projectile.Rb.AddComponent<Explosive>();
            explosive.damage = explosionDamage;
            explosive.radius = explosionRadius;
            explosive.explosion = explosion;
            
            projectile.OnEndOfLife += explosive.Explode;

        }
    }
}