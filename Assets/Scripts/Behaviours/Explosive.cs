﻿using CowChow.GameJam2023.Utilities;
using UnityEngine;

namespace CowChow.GameJam2023.Behaviours
{
    public class Explosive : MonoBehaviour
    {
        public float radius = 5f;
        public int damage = 1;

        public GameObject explosion;
        
        [SerializeField] private float explosionDestructionDelay = 2;

        private void OnDrawGizmos()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, radius);
        }


        public void Explode()
        {            
            Collider[] colliders = Physics.OverlapSphere(transform.position, radius);

            foreach (Collider collider in colliders)
            {
                if (collider.GetComponent<IDamagable>() != null)
                {
                    collider.GetComponent<IDamagable>().TakeDamage(damage);
                }
            }

            if (!explosion)
            {
                return;
            }

            GameObject newExplosion = Instantiate(explosion, transform.position, Quaternion.identity);
            newExplosion.transform.localScale *= radius;
            newExplosion.AddComponent<DelayedSelfDestruct>().destroyDelay = explosionDestructionDelay;
        }
    }
}