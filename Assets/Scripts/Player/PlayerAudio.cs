using UnityEngine;

public class PlayerAudio : MonoBehaviour
{
    // --- | Inspector | ---------------------------------------------------------------------------------------------------

    [Header("Dash")] // ---------------------------------------------------------------------------
    [SerializeField, Tooltip("The source the dash sound will be played through.")]
    private AudioSource dashSource = default;

    [SerializeField, Tooltip("The collection of sounds that can be played when dashing.")]
    private SoundCollection dashSounds = null;


    // --- | Methods | -----------------------------------------------------------------------------------------------------

    /// <summary>
    /// Plays the dash sound.
    /// </summary>
    public void Dash()
    {
        if (dashSounds.Count == 0)
        {
            Debug.LogWarning("No dash sounds assigned.");
            return;
        }
        dashSource.PlayOneShot(dashSounds.GetRandom());
    }
}
