using System;
using System.Collections;
using UnityEngine;

public class FrogBullet : MonoBehaviour
{
    [SerializeField] private int _damage;
    [SerializeField] private float _speed;
    [SerializeField] private Rigidbody _rigidbody;
    
    public void Init(int damage, float speed)
    {
        _damage = damage;
        _speed = speed;
    }

    private void Start()
    {
        StartCoroutine(DestroyTimer());
        _rigidbody.AddForce(transform.forward * _speed * 100);
    }

    // Update is called once per frame
    private void Update()
    {
        //transform.Translate(Vector3.forward * (_speed * Time.deltaTime));
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            var playerDamageComponent = collision.gameObject.GetComponent<IDamagable>();
            playerDamageComponent?.TakeDamage(_damage);   
        }
        
        Destroy(gameObject);
    }

    private IEnumerator DestroyTimer()
    {
        yield return new WaitForSeconds(5f);
        Destroy(gameObject);
    }
}
