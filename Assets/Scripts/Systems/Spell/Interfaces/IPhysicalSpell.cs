﻿using UnityEngine;

namespace CowChow.GameJam2023
{
    public interface IPhysicalSpell
    {
        Transform Origin { get; set; }
    }
}