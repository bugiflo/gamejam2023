﻿using System;
using UnityEngine;

namespace CowChow.GameJam2023
{
    public interface IProjectile
    {
        Rigidbody Rb { get; set; }

        void SetUpProjectileRb();
        
        float Lifetime { get; set; }

        float LaunchForce { get; set; }
        
        void Shoot(Vector3 direction);

        event Action OnEndOfLife;

        event Action<Collision> OnProjectileCollision;
    }
}