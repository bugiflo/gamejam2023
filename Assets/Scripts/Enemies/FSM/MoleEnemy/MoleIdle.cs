using System.Collections;
using UnityEngine;

public class MoleIdle : BaseState
{
    private MoleMovementSM _sm;

    public MoleIdle(MoleMovementSM stateMachine) : base("Idle", stateMachine)
    {
        _sm = stateMachine;
    }

    public override void Enter()
    {
        base.Enter();
        _sm.Animator.SetTrigger("Hide");
        _sm.Collider.enabled = false;
      
        _sm.StartCoroutine(IdleRoutine());
    }


    private IEnumerator IdleRoutine()
    {
        yield return new WaitForSeconds(_sm.IdlerTimer);
        
        _sm.Collider.enabled = true;
        
        _sm.ChangeState(_sm.Attack);
    }
}
