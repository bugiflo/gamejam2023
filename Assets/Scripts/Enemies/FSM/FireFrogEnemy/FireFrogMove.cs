using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class FireFrogMove : BaseState
{
    private readonly FireFrogMovementSM _sm;
    private Vector3 _randomPositionToMove;
    private readonly Vector3 _startScale;
    private float _travelDistance;

    private readonly float _oldSpeed;
    private bool _hasPath;

    public FireFrogMove(FireFrogMovementSM stateMachine) : base("Move", stateMachine)
    {
        _sm = stateMachine;
        _startScale = _sm.AvatarPivot.transform.localScale;
        _oldSpeed = _sm.Agent.speed;
    }
    
    private Vector3 FindNewPosition()
    {
        var randomPositionInArea = GetPointInBounds();
        NavMesh.SamplePosition(randomPositionInArea, out var hit, 100, NavMesh.AllAreas);
    
        return hit.position;
    }
    
    private Vector3 GetPointInBounds()
    {
        while (true)
        {
            var position = _sm.transform.position;
            var randomDir = Random.insideUnitSphere;

            randomDir = new Vector3(randomDir.x * _sm.MaxJumpDistance, _sm.StartHeight, randomDir.x * _sm.MaxJumpDistance);
            randomDir = new Vector3(position.x + randomDir.x, randomDir.y, position.z + randomDir.z);

            var bounds = _sm.SpawnArea.bounds;
            if (!bounds.Contains(randomDir)) continue;
            return randomDir;
            break;
        }
    }
    
    public override void Enter()
    {
        _sm.StartCoroutine(MoveRoutine());
    }
    
    private IEnumerator MoveRoutine()
    {
        var randomPositionToMove = FindNewPosition();
        _sm.Agent.SetDestination(randomPositionToMove);
        
        yield return new WaitWhile(() => _sm.Agent.pathPending);
        
        var travelDistance = _sm.Agent.remainingDistance;
        
        // Set speed according to distance.
        var speedAddition = Mathf.Lerp(1, 2, travelDistance);
        _sm.Agent.speed = _oldSpeed * speedAddition;
        
        _sm.Animator.SetTrigger("Jump");
        _sm.Col.enabled = false;

        var agent = _sm.Agent;
        
        yield return new WaitUntil(() =>
        {
            var remainingDistance = agent.remainingDistance;

            var evaluatedCurve = _sm.JumpCurve.Evaluate(1f - (remainingDistance / travelDistance));

            var position = _sm.transform.position;
            var newPivotPosition = new Vector3(position.x, _sm.StartHeight + evaluatedCurve,
                position.z);
            var newShadowScale = new Vector3(_startScale.x * (1 + evaluatedCurve), _startScale.y * (1 + evaluatedCurve),
                _startScale.z * (1 + evaluatedCurve));

            _sm.AvatarPivot.transform.position = newPivotPosition;
            _sm.DropShadow.transform.localScale = newShadowScale;

            return !float.IsInfinity(remainingDistance) && agent.pathStatus == NavMeshPathStatus.PathComplete &&
                   agent.remainingDistance == 0;
        });
        
        _sm.Agent.speed = _oldSpeed;
        _sm.Col.enabled = true;
        _sm.Animator.SetTrigger("Idle");
        
        var choice = Random.Range(0, 6);

        if (choice is >= 0 and <= 3)
        {
            _sm.ChangeState(_sm.NormalAttack);
        }
        else
        {
            _sm.ChangeState(_sm.SpecialAttack);
        }
    }
}
