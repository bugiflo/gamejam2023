
using UnityEngine;
using UnityEngine.AI;

public class FireFrogMovementSM : StateMachine
{
    public FireFrogIdle Idle;
    
    public FireFrogMove Move;
    
    public FireFrogNormalAttack NormalAttack;
    
    public FireFrogSpecialAttack SpecialAttack;
    

    public void InitFireFrogMovementSM(
        NavMeshAgent agent,
        Collider spawnArea,
        Transform player,
        GameObject bullet,
        ParticleSystem nova,
        Animator animator,
        AnimationCurve jumpCurve,
        AnimationCurve specialAttackCurve,
        GameObject avatarPivot,
        GameObject dropShadow,
        float specialAttackRadius,
        float specialAttackSpeed, 
        int specialAttackAmount,
        Collider col, 
        AudioSource source,
        float maxJumpDistance)
    {
        Agent = agent;
        SpawnArea = spawnArea;
        Player = player;
        Bullet = bullet;
        Nova = nova;
        Animator = animator;
        JumpCurve = jumpCurve;
        SpecialAttackCurve = specialAttackCurve;
        AvatarPivot = avatarPivot;
        DropShadow = dropShadow;
        SpecialAttackRadius = specialAttackRadius;
        SpecialAttackSpeed = specialAttackSpeed;
        SpecialAttackAmount = specialAttackAmount;
        StartHeight = transform.position.y;
        Col = col;
        Source = source;
        MaxJumpDistance = maxJumpDistance;

        Idle = new FireFrogIdle(this);
        Move = new FireFrogMove(this);
        NormalAttack = new FireFrogNormalAttack(this);
        SpecialAttack = new FireFrogSpecialAttack(this);
    }

    protected override BaseState GetInitialState()
    {
        return Idle;
    }
    
    public GameObject InstantiateObject(GameObject obj)
    {
        return Instantiate(obj);
    }
    
    public NavMeshAgent Agent { get; set; }

    public Collider SpawnArea { get; set; }

    public Transform Player { get; set; }
    
    public GameObject Bullet { get; set; }
    
    public ParticleSystem Nova { get; set; }

    public Animator Animator { get; set; }

    public AnimationCurve JumpCurve { get; set; }
    
    public AnimationCurve SpecialAttackCurve { get; set; }

    public GameObject AvatarPivot { get; set; }

    public GameObject DropShadow { get; set; }
    
    public float SpecialAttackRadius { get; set; }
    
    public float SpecialAttackSpeed { get; set; }
    
    public int SpecialAttackAmount { get; set; }
    
    public float StartHeight { get; set; }
    public Collider Col { get; set; }
    public AudioSource Source { get; set; }
    
    public float MaxJumpDistance { get; set; }
}
